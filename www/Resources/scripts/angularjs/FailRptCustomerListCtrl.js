CPApp.controller("FailRptCustomerListCtrl", function(
  $scope,
  $stateParams,
  $state,
  $timeout,
  $ionicLoading,
  FailReportService,
  TMLTHService
) {
  // Get stateParams
  $scope.strascode = $stateParams.ascode;
  var current_agent = $scope.strascode

  // Setup Loading
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 10
  });

  $scope.txtPageLoad = "Loading...";

  FailReportService.getCustomer(current_agent).then(
    function(CustMem) {
      //users is an array of user objects

      //console.log(CustMem);

      $scope.ListCust = CustMem;
      $scope.txtPageLoad =
        CustMem == null || CustMem.length == 0
          ? strPaymentType == "D"
            ? "ไม่พบรายการหักบัญชีธนาคารไม่ผ่าน"
            : "ไม่พบรายการตัดบัตรเครดิตไม่ผ่าน"
          : "";

      $timeout(function() {
        $ionicLoading.hide();
      }, 3000);
    },
    function(error) {
      //Something went wrong!
      
      if (error.status == 401) {
        TMLTHService.unauthorizedService();
      } else {
        $scope.txtPageLoad = "Something went wrong!";
      }
      $timeout(function() {
        $ionicLoading.hide();
      }, 3000);
    }
  );
  $scope.listlength = 500;

  $scope.ViewFailReportDetail = function(val) {
    fnGotoDetail(val);
  };
  function fnGotoDetail(val) {

    $state.go("app.FailRptCustomerDetail", { Chdrnum: val, Ascode: current_agent });
  }
});
