CPApp.controller("FailRptAgentListCtrl", function(
  $scope,
  $stateParams,
  $state,
  $timeout,
  $ionicLoading,
  FailReportService,
  TMLTHService
) {
  // Get stateParams
  $scope.sUnitco = $stateParams.unitco;

  // Setup Loading
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 10
  });

  if ($scope.sUnitco == null || $scope.sUnitco == "") {
    $scope.sUnitco = strUnitco;
  }

  $scope.txtTitle =
  strPaymentType == "D"
      ? "รายชื่อตัวแทนที่มีรายการหักบัญชีธนาคารไม่ผ่าน"
      : "รายชื่อตัวแทนที่มีรายการตัดบัตรเครดิตไม่ผ่าน";

  var sUnitco = "";
  sUnitco = $scope.sUnitco;
  strSelectedUnit = sUnitco;


  FailReportService.getAgent(sUnitco).then(
    function(users) {
      //users is an array of user objects
      //console.log(users);
      $scope.ListAgentUnderCtrl =  users;

      $scope.AgentMember = users;

      $scope.txtPageLoad =
        users == null || users.length == 0
          ? strPaymentType == "D"
            ? "ไม่พบรายการหักบัญชีธนาคารไม่ผ่าน"
            : "ไม่พบรายการตัดบัตรเครดิตไม่ผ่าน"
          : "";

      $timeout(function() {
        $ionicLoading.hide();
      }, 3000);
    },
    function(error) {
      //Something went wrong!
      if (error.status == 401) {
        TMLTHService.unauthorizedService();
      } else {
        $scope.txtPageLoad = "Something went wrong!";
      }

      $timeout(function() {
        $ionicLoading.hide();
      }, 3000);
    }
  );

  $scope.loadOrder = function loadOrder(ObjData) {
    strSelectedASCodeAgent = ObjData;
    $state.go("app.FailRptCustomerList", { ascode: ObjData });
  };

  //$scope.friends = Friends.all();
  $scope.listlength = 200;
});
