CPApp.controller("FailRptCustomerDetailCtrl", function(
  $scope,
  $stateParams,
  $timeout,
  $ionicLoading,
  FailReportService,
  TMLTHService
) {
  // Get stateParams
  $scope.sChdrnum = $stateParams.Chdrnum;
  $scope.sAscode = $stateParams.Ascode;

  //alert("Hi");

  // Setup Loading
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 10
  });

  $scope.txtPageLoad = "";
  //$scope.txtChdrnum = "Loading...";

  FailReportService.getFailReportDetail($scope.sChdrnum, $scope.sAscode).then(
    function(FailData) {
      //users is an array of user objects

      $scope.FailReportDetail = FailData[0];
      
      $scope.telPayerTel = FailData[0].PAYER_TEL == "-" ? FailData[0].PAYER_TEL : "<a href='tel: "+FailData[0].PAYER_TEL+"' class='CTCText'>"+ FailData[0].PAYER_TEL+ "</a>";
      $scope.telCallCenter = "<a href='tel: "+FailData[0].CALLCENTER_TEL+"' class='CTCText'>"+ FailData[0].CALLCENTER_TEL+ "</a>";

      //console.log($scope.FailReportDetail)

      $timeout(function() {
        $ionicLoading.hide();
      }, 3000);
    },
    function(error) {
      //Something went wrong!
      if (error.status == 401) {
        TMLTHService.unauthorizedService();
      } else {
        $scope.txtPageLoad = "Something went wrong!";
      }
      $timeout(function() {
        $ionicLoading.hide();
      }, 3000);
    }
  );

});
