CPApp.controller("CustomerListCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  $ionicLoading,
  MemberAgentCustomer,
  TMLTHService,
  EncryptDecryptData
) {
  // console.log("CustomerListCtrl");

  $scope.strascode = $stateParams.ascode;

  try {
    $scope.txtPageLoad = "Loading...";

    MemberAgentCustomer.getAGCMem($scope.strascode).then(
      function(CustMem) {
        //users is an array of user objects
        $scope.ListCust = CustMem;
        $scope.txtPageLoad =
          CustMem == null || CustMem.length == 0 ? "ไม่พบรายการลูกค้า" : "";
      },
      function(error) {
        //Something went wrong!
        alert("Error MemberAgentCustomer");
      }
    );

    //$scope.friends = Friends.all();
    $scope.listlength = 500;
  } catch (err) {
    alert("Error : " + err.message);
  }

  function fnGetCustByAgent(ASCodeVal) {
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    fnGetAllCustByAgent(ASCodeVal);
  }

  function fnGetAllCustByAgent(ASCodeVal) {
    try {
      $http({
        url: URLAG + "/GetAllCustByAgentASCode_New",
        method: "POST",
        data: { strASCode: EncryptDecryptData.Encrypt(ASCodeVal) }
      }).then(
        function(response) {
          $timeout(function() {
            $ionicLoading.hide();
          }, 1000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.ViewInsure = function(val) {
    fnViewInsure(val);
  };
  function fnViewInsure(val) {
    //alert('fnViewInsure:' + val);
    strClntnum = val;
    strClntnumVw = val;

    $state.go("app.InsureInfoVw", { clntNo: val });
    //$state.go('app.Hospital');
  }

  $scope.ViewPaymentLog = function(val) {
    fnViewPaymentLog(val);
  };
  function fnViewPaymentLog(val) {
    strClntnum = val;
    strClntnumVw = val;

    $state.go("app.PaymentLogVw");
  }
});

CPApp.factory("MemberAgentCustomer", function($http,EncryptDecryptData) {
  var AgentCustomerMemberListX = [
    { id: 0, Asname: "Silver JK" },
    { id: 1, Asname: "Ruby JK" },
    { id: 2, Asname: "PEARL JK" },
    { id: 3, Asname: "GP JK" },
    { id: 4, Asname: "PT JK" },
    { id: 5, Asname: "EM JK" },
    { id: 6, Asname: "DI JK" },
    { id: 7, Asname: "EDC JK" }
  ];

  var AgentCustomerMemberList = [];

  return {
    all: function() {
      return AgentCustomerMemberListX;
    },
    get: function(strASCD) {
      // Simple index lookup
      return AgentCustomerMemberListX[strASCD];
    },
    getAGCMem: function(strASCD) {
      // alert(strLVLCo + "," + strUntCo);
      // alert(URLAG);
      // alert(strASCD);

      return $http
        .post(URLAG + "/GetAllCustByAgentASCode_New", { strASCode: EncryptDecryptData.Encrypt(strASCD) })
        .then(function(response) {
          //alert('success');
          AgentCustomerMemberList = response.data.d;
          return AgentCustomerMemberList;
        });
    }
  };
});
