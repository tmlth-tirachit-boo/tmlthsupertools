/***************************** App Detail ********************************/

var lng = "TH";
var AppName = "touch point";
var AppClient = "agent";

var AppID = "";
if (ionic.Platform.platform() == "ios") {
  AppID = "0DC16627-65A2-4680-8885-E756AFDA9298";
} else {
  AppID = "CAA1414A-1B81-402B-B006-230A754DF09D";
}

// let AppVer = "1.6.0" // Fix pentest 2019 - round 2
// const AppVer = "1.7.0" // Add popup image privacy policy
// const AppVer = "1.7.1" // Fix minor bug
const AppVer = "1.8.0" // Common Insured Card
const urlPDP = "https://www.tokiomarinelife.co.th/agweb/th_ag_personal_data_policy.html"

/*************************** URL Service *********************************/
// Production
// var URLGetFile = "https://www.tokiomarinelife.co.th/touchpoint/";
// var URLServe = "https://www.tokiomarinelife.co.th/touchpoint/";
// DR site
// var URLGetFile = "https://dr02.tokiomarinelife.co.th/touchpoint/";
// var URLServe = "https://dr02.tokiomarinelife.co.th/touchpoint/";
// UAT
var URLGetFile = "https://staging.tokiomarinelife.co.th/touchpoint/";
var URLServe = "https://staging.tokiomarinelife.co.th/touchpoint/";
// UAT Common Insured Card
// var URLGetFile = "https://staging.tokiomarinelife.co.th/touchpoint_InsuredCard/";
// var URLServe = "https://staging.tokiomarinelife.co.th/touchpoint_InsuredCard/";
// Test New Server
// var URLGetFile = "https://staging2.tokiomarinelife.co.th/touchpoint/";
// var URLServe = "https://staging2.tokiomarinelife.co.th/touchpoint/";
// Pentest
// var URLGetFile = "https://staging.tokiomarinelife.co.th/touchpoint_pentest/";
// var URLServe = "https://staging.tokiomarinelife.co.th/touchpoint_pentest/";

// Common insured website
var URLCommonCardWebsite = 'https://www.tokiomarinelife.co.th/CommonCard/InsuredCard/Payment'
// var URLCommonCardWebsite = 'https://staging.tokiomarinelife.co.th/CommonCard/InsuredCard/Payment'

var URLInsuredCard = URLServe + "wsInsure_Card.asmx";
var URLAuth = URLServe + "wsAuthorization_Authen.asmx";
var URLNews = URLServe + "wsNewsV2_Authen.asmx";
var URLBranch = URLServe + "wsBranch_Authen.asmx";
var URLContactUs = URLServe + "wsContactUs_Authen.asmx";
var URLLookup = URLServe + "wsHospital_Authen.asmx";
var URLInsure = URLServe + "wsInsurance_Authen.asmx";
var URLOtp = URLServe + "wsOTP_Authen.asmx";
var URLRegister = URLServe + "wsAuthorization_Authen.asmx";
var URLFtp = URLServe + "wsFTPService_Authen.asmx";
var URLSendMail = URLServe + "wsEmail_Authen.asmx";
var URLDownload = URLServe + "wsDownload_Authen.asmx";
var URLAG = URLServe + "wsAgent_Authen.asmx";
var URLCommon = URLServe + "wsCommon_Authen.asmx";
var URLInvestment = URLServe + "wsInvestment_Authen.asmx";

/*************************** Before Pen test ***********************************/
//var URLServe = 'http://testweb1/CustomerPortal/'
//var URLServe = 'https://staging.tokiomarinelife.co.th/TouchPoint/'
//var URLServe = 'https://www.tokiomarinelife.co.th/touchpoint/'
//var URLServe = 'https://www.tokiomarinelife.co.th/TouchPointReserve/'
//var URLServe = 'https://dr02.tokiomarinelife.co.th/touchpoint/'
// var URLAuth = URLServe + "wsAuthorization.asmx";
// var URLNews = URLServe + "wsNewsV2.asmx";
// var URLBranch = URLServe + "wsBranch.asmx";
// var URLContactUs = URLServe + "wsContactUs.asmx";
// var URLLookup = URLServe + "wsHospital.asmx";
// var URLInsure = URLServe + "wsInsurance.asmx";
// var URLOtp = URLServe + "wsOTP.asmx";
// var URLRegister = URLServe + "wsAuthorization.asmx";
// var URLFtp = URLServe + "wsFTPService.asmx";
// var URLSendMail = URLServe + "wsEmail.asmx";
// var URLDownload = URLServe + "wsDownload.asmx";
// var URLAG = URLServe + "wsAgent.asmx";
// var URLCommon = URLServe + "wsCommon.asmx";

/*****************************************************************************/

var ActRegister = "731E8119-C767-447E-A2A8-8D8FF8D44CDA";
var ActForgotPWD = "B3718FD1-7146-4827-BF25-EA0811360501";

var PaymentDoctypePOS014 = "POS014"; /*ใบรับรองการชำระเบี้ย*/
var PaymentDoctypePOS037 = "POS037"; /*ใบเสร็จรับเงินเบี้ยประกันภัย*/
var PaymentDoctypePOS001 = "POS001"; /*ใบแจ้งครบกำหนดชำระเบี้ย*/

var varMailFrom = "csc@tokiomarinelife.co.th";

function TestNa() {
  alert("TestNa");
}

/******************************************************************************/

/*var strIDCard='';
var strClntnum = '';
var strChdrnum = '';
var strQuestion = '';
var strAnswer = '';
var strPassword = '';
var strUserid = '';
var strName_ins = '';
var strLastname_ins = '';
var strMobile_no = '';
var dtMobile_dt = '';
var strPrev_mobile = '';
var strEmail = '';
var dtEmail_dt = '';
var strPrev_email = '';
var strAgreement = '';
var dtUpddate = '';
var dtRegister_dt = '';
var strPhone = '';
var strPrev_phone = '';
var dtPhone_dt = '';
var strReset_password = '';
var guidReset_code = '';
var dtReset_date = '';
var strSend_life_asia = '';
var dtSend_dt = '';

var strUserImagePath = '';
var strUniqueCode = '';*/
