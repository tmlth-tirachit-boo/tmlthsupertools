//CPApp.controller('MemberListCtrl', function($scope,$stateParams,$http, $state,$timeout, $ionicLoading, Friends,MemberAgent)
CPApp.controller("AgentListCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  MemberAgent,
  TMLTHService,
  EncryptDecryptData
) {
  $scope.sUnitco = $stateParams.unitco;
  if ($scope.sUnitco == null || $scope.sUnitco == "") {
    $scope.sUnitco = strUnitco;
  }

  var sUnitco = "";
  sUnitco = $scope.sUnitco;

  strSelectedUnit = sUnitco;

  MemberAgent.getAgentMem(sUnitco).then(
    function(users) {
      //users is an array of user objects
      $scope.AgentMember = users;
    },
    function(error) {
      //Something went wrong!
      if (error.status == 401) {
        TMLTHService.unauthorizedService();
      } else {
        alert("Fail");
      }
    }
  );

  $scope.loadOrder = function loadOrder(ObjData) {
    strSelectedASCodeAgent = ObjData;

    $state.go("app.CustomerList", { ascode: ObjData });
  };

  //$scope.friends = Friends.all();
  $scope.listlength = 200;

  /******************************/

  //alert(sUnitco);
  try {
    let encrypt_sUnitco = EncryptDecryptData.Encrypt(sUnitco);
    console.log(encrypt_sUnitco);
    $http({
      url: URLAG + "/GetAgentUnderControl_min_New",
      method: "POST",
      data: { strUnitco: encrypt_sUnitco }
    }).then(
      function(response) {
        //alert('success');

        $scope.ListAgentUnderCtrl = response.data.d;
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Fail");
        }
      }
    );
  } catch (err) {
    alert("Error GetAgentUnderControl_min_New : " + err.message);
  }
});

CPApp.factory("MemberAgent", function($http,EncryptDecryptData) {
  var AgentMemberListX = [
    { id: 0, Asname: "Silver JK" },
    { id: 1, Asname: "Ruby JK" },
    { id: 2, Asname: "PEARL JK" },
    { id: 3, Asname: "GP JK" },
    { id: 4, Asname: "PT JK" },
    { id: 5, Asname: "EM JK" },
    { id: 6, Asname: "DI JK" },
    { id: 7, Asname: "EDC JK" }
  ];

  var AgentMemberList = [];

  //alert('XXX');

  return {
    all: function() {
      return AgentMemberListX;
    },
    get: function(AGMemID) {
      // Simple index lookup
      return AgentMemberListX[AGMemID];
    },
    getAgentMem: function(strUnitco) {
      //alert(strLVLCo + "," + strUntCo);
      //alert(URLAG);
      //alert(strUnitco);

      let encrypt_sUnitco = EncryptDecryptData.Encrypt(strUnitco);
      // console.log(encrypt_sUnitco);

      return $http
        .post(URLAG + "/GetAgentUnderControl_min_New", { strUnitco: encrypt_sUnitco })
        .then(function(response) {
          //alert('pass');
          AgentMemberList = response.data.d;
          //alert(AgentMemberList[0].Asname);
          //AgentMemberList = response;
          return AgentMemberList;
        });
    }
  };
});
