CPApp.controller("FailRptUnitListCtrl", function(
  $scope,
  $state,
  FailReportService,
  TMLTHService
) {
  strSelectedASCode63 = strASCode;
  FailReportService.getUnit(strASCode).then(
    function(users) {
      //users is an array of user objects
      $scope.UnitMember = users;
      $scope.ListUnitUnderCtrl = users;
    },
    function(error) {
      if (error.status == 401) {
        TMLTHService.unauthorizedService();
      } else {
        alert("Error");
      }
    }
  );

  $scope.loadOrder = function loadOrder(ObjData) {
    //alert(ObjData);
    $state.go("app.FailRptAgentList", { unitco: ObjData });
  };

  //$scope.friends = Friends.all();
  $scope.listlength = 200;

  // /******************************/

  // try {
  //   $http({
  //     url: URLAG + "/GetUnitUnderControl_min",
  //     method: "POST",
  //     data: { strAscode: strASCode }
  //   }).then(
  //     function(response) {
  //       //alert('success');

  //       $scope.ListUnitUnderCtrl = response.data.d;
  //     },
  //     function(response) {
  //       // optional
  //       // failed
  //       alert("Fail : GetUnitUnderControl_min");
  //       alert(response);
  //       debugger;
  //     }
  //   );
  // } catch (err) {
  //   alert("Error GetUnitUnderControl_min : " + err.message);
  // }
});
