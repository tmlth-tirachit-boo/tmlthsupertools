//CPApp.controller('MemberListCtrl', function($scope,$stateParams,$http, $state,$timeout, $ionicLoading, Friends,MemberAgent)
CPApp.controller("UnitListCtrl", function(
  $scope,
  $http,
  $state,
  MemberUnit,
  TMLTHService,
  EncryptDecryptData
) {
  strSelectedASCode63 = strASCode;

  MemberUnit.getUnitMem(strASCode).then(
    function(users) {
      //users is an array of user objects
      $scope.UnitMember = users;
    },
    function(error) {
      //Something went wrong!
      if (error.status == 401) {
        TMLTHService.unauthorizedService();
      } else {
        alert("Error");
      }
    }
  );

  $scope.loadOrder = function loadOrder(ObjData) {
    //alert(ObjData);
    $state.go("app.AgentList", { unitco: ObjData });
  };

  //$scope.friends = Friends.all();
  $scope.listlength = 200;

  /******************************/

  try {
    let encrypt_ASCode = EncryptDecryptData.Encrypt(strASCode);
    // console.log(encrypt_ASCode);

    $http({
      url: URLAG + "/GetUnitUnderControl_min_New",
      method: "POST",
      data: { strAscode: encrypt_ASCode }
    }).then(
      function(response) {
        $scope.ListUnitUnderCtrl = response.data.d;
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Error");
        }
      }
    );
  } catch (err) {
    alert("Error GetUnitUnderControl_min_New : " + err.message);
  }
});

CPApp.factory("MemberUnit", function($http,EncryptDecryptData) {
  var UnitMemberListX = [
    { id: 0, Asname: "Silver JK" },
    { id: 1, Asname: "Ruby JK" },
    { id: 2, Asname: "PEARL JK" },
    { id: 3, Asname: "GP JK" },
    { id: 4, Asname: "PT JK" },
    { id: 5, Asname: "EM JK" },
    { id: 6, Asname: "DI JK" },
    { id: 7, Asname: "EDC JK" }
  ];

  var UnitMemberList = [];

  //alert('XXX');

  return {
    all: function() {
      return UnitMemberListX;
    },
    get: function(AGMemID) {
      // Simple index lookup
      return UnitMemberListX[AGMemID];
    },
    getUnitMem: function(strASCode) {
      let encrypt_ASCode = EncryptDecryptData.Encrypt(strASCode);
      // console.log(encrypt_ASCode);

      return $http
        .post(URLAG + "/GetUnitUnderControl_min_New", { strAscode: encrypt_ASCode })
        .then(function(response) {
          //alert('pass');
          UnitMemberList = response.data.d;
          //alert(AgentMemberList[0].Asname);
          //AgentMemberList = response;
          return UnitMemberList;
        });
    }
  };
});
