CPApp.controller("HomeCtrl", function(
  $scope,
  $http,
  $timeout,
  $ionicLoading,
  $sce,
  TMLTHService,
  JWTService,
  $ionicModal,
  $rootScope
) {
  // alert($rootScope.showPDB)

  var today = new Date(
    new Date().getFullYear(),
    new Date().getMonth(),
    new Date().getDate()
  );
  var target = new Date(2020, 4, 28);

  console.log(today);
  console.log(target);
  // alert(today + "<:>" +target)

  let showPopup;

  if (today >= target) {
    showPopup = false;
  } else {
    showPopup = true;
  }

  $ionicModal
    .fromTemplateUrl("my-modal.tmpl.html", {
      scope: $scope,
      animation: "slide-in-up"
    })
    .then(function(modal) {
      $scope.modal = modal;

      $scope.openModal = function() {
        $rootScope.showPDB = false;
        $scope.modal.show();
      };

      $scope.closeModal = function() {
        $scope.modal.hide();
      };

      $scope.$on("$destroy", function() {
        $scope.modal.remove();
      });

      if ($rootScope.showPDB && showPopup) {
        $scope.openModal();
      }
    });

  $scope.gotoPDPWebsite = function() {
    window.open(encodeURI(urlPDP), "_blank", "location=yes");
  };

  //Agent TP
  $scope.User = strASName + " " + strASSName;

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  try {
    $http({
      url: URLCommon + "/GetNotificationMessages",
      method: "POST",
      data: { strAppID: AppID, strClntnum: "", strPlatform: "SuperTools" }
    }).then(
      function(response) {
        $scope.ListNotificationMessages = response.data.d;
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Error");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  $scope.trustAsHtml = function(string) {
    var result = "";
    if (string != null || string != "") {
      result = $sce.trustAsHtml(string);
    }
    return result;
  };

  //  var strBthDay = '';
  //  strBthDay = strCltdob_ins;

  //  var strToday = '';

  //  var today = new Date();
  //  var dd = today.getDate();
  //  var mm = today.getMonth()+1; //January is 0!

  //  var yyyy = today.getFullYear();
  //  if(dd<10){
  //  dd='0'+dd
  //  }
  //  if(mm<10){
  //  mm='0'+mm
  //  }
  //  var strToday = yyyy+''+mm+''+dd;

  //  //alert(strBthDay);
  //  //alert(strToday);

  //  strBthDay = strBthDay.substring(4);
  //  strToday=strToday.substring(4);

  //  //alert(strBthDay);
  //  //alert(strToday);

  //  //For Test//
  //  //strToday = "0828";
  //  //For Test//

  //  if(strBthDay == strToday)
  //  {
  //     $scope.TextHBD="<br /><div><h4 class='HBD_h4'><span class='HBD_SPAN'>******************</span></h4><h4 class='HBD_h4'><span class='HBD_SPAN'>** สุขสันต์วันเกิด **</span></h4><h4 class='HBD_h4'><span class='HBD_SPAN'>บริษัทขอร่วมยินดีและขอให้ท่าน</span></h4><h4 class='HBD_h4'><span class='HBD_SPAN'>มีความสุข สดชื่นในวันพิเศษนี้</span></h4><h4 class='HBD_h4'><span class='HBD_SPAN'>******************</span></h4></div>";
  //  }
  //  else
  //  {
  //     $scope.TextHBD="";
  //  }

  // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
  $timeout(function() {
    $ionicLoading.hide();
  }, 3000);
});
