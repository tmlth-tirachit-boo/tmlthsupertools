CPApp.controller("UnitLinkCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  $ionicLoading
) {
  // $ionicLoading.show({
  //   content: "Loading",
  //   animation: "fade-in",
  //   showBackdrop: true,
  //   maxWidth: 20
  //   showDelay: 0
  // });

  //Param To News Detail
  $scope.chdrnum = $stateParams.chdrnum;
  $scope.product_code = $stateParams.product_code;
  $scope.start_agreement = str_start_agreement;
  $scope.end_agreement = str_end_agreement;

  fnGetInvestment($scope.chdrnum, $scope.product_code);

  function fnGetInvestment(_chdrnum, _product_code) {

    try {
      $http({
        url: URLInvestment + "/GetInvestment",
        method: "POST",
        data: { chdrnum: _chdrnum, product_code: _product_code }
      }).then(
        function(response) {
          // console.log(response.data.d);          

          if (response.data.d.length == 0) {
            $scope.MsgNoData = "ไม่มีรายการข้อมูล";
          } else {
            $scope.ListInvestment = response.data.d;

            var strPrdCode = $scope.ListInvestment[0].PRODUCT_CODE
            var strPrdSName = $scope.ListInvestment[0].PRODUCT_SHOT_NAME
            var strPrdLName = $scope.ListInvestment[0].PRODUCT_LONG_NAME
            $scope.Title = strPrdCode.concat(' ',strPrdSName,' ',strPrdLName)
          }

          // $timeout(function() {
          //   $ionicLoading.hide();
          // }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.ChkNegativeNumber = function(value) {
    
    // let intValue = parseInt(value)
    // console.log(value)
    let intValue = parseFloat(value)
    
    let result = intValue < 0 ? 'Y' : 'N'
                 
    return result

  }

});

CPApp.controller("UnitLinkDetailCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  $ionicLoading
) {
  // $ionicLoading.show({
  //   content: "Loading",
  //   animation: "fade-in",
  //   showBackdrop: true,
  //   maxWidth: 200,
  //   showDelay: 0
  // });

  //Param To News Detail
  //  debugger;
  $scope.chdrnum = $stateParams.chdrnum;
  $scope.date_from = $stateParams.date_from;
  $scope.date_to = $stateParams.date_to;
  $scope.coverage = $stateParams.coverage;
  $scope.fund_code = $stateParams.fund_code;
  
  fnGetInvestment($scope.chdrnum,$scope.date_from,$scope.date_to,$scope.coverage,$scope.fund_code)
  
  function fnGetInvestment(_chdrnum, _date_from,_date_to,_coverage,_fund_code) {

    try {
      $http({
        url: URLInvestment + "/GetInvestmentTranSection",
        method: "POST",
        data: { chdrnum: _chdrnum, date_from: _date_from, date_to: _date_to, coverage: _coverage, fund_code: _fund_code }
      }).then(
        function(response) {
          // console.log(response.data.d);          

          if (response.data.d.length == 0) {
            $scope.MsgNoData = "ไม่มีรายการข้อมูล";
          } else {
            $scope.ListInvestmentTran = response.data.d;

            $scope.FUND_CODE = $scope.ListInvestmentTran[0].FUND_CODE
            var strPrdSName = $scope.ListInvestmentTran[0].ASSET_SHOT_NAME
            var strPrdLName = $scope.ListInvestmentTran[0].ASSET_LONG_NAME
            $scope.Title = strPrdLName.concat(' : ',strPrdSName)
          }

          // $timeout(function() {
          //   $ionicLoading.hide();
          // }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            $scope.MsgNoData = "ไม่มีรายการข้อมูล";
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.ChkNegativeNumber = function(value) {
    
    // let intValue = parseInt(value)
    // console.log(value)
    let intValue = parseFloat(value)
    
    let result = intValue < 0 ? 'Y' : 'N'
                 
    return result

  }


});
