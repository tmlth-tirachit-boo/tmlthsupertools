CPApp.controller("PaymentLogVwCtrl", function(
  $scope,
  $http,
  $timeout,
  $ionicLoading,
  $ionicModal,
  TMLTHService,
  EncryptDecryptData,
  $ionicHistory,
  $state
) {
  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  //************* Add fnGetDataAsOf by tirachit.boo *****************//
  fnGetDataAsOf();

  function fnGetDataAsOf() {
    try {
      $http({
        url: URLCommon + "/GetAppVersionService",
        method: "POST",
        data: { strAppID: AppID, strInputVer: AppVer }
      }).then(
        function(response) {
          $scope.latesdDataDate = response.data.d[0].AppLatestDataDateText;
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error GetAppVersionService: " + err.message);
    }
  }

  $scope.HasData = "Y";

  function fnGetInsureCardInfo(val) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      $http({
        url: URLInsure + "/GetInsuranceCard_New",
        method: "POST",
        data: {
          strPolicyNo: EncryptDecryptData.Encrypt(val),
          strLng: EncryptDecryptData.Encrypt(lng)
        }
      }).then(
        function(response) {
          $scope.InsuranceCardInfo = "";
          $scope.InsuranceCardInfo = response.data.d[0];

          // $timeout(function() {
          //   $ionicLoading.hide();
          // }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  var strAsCodeSelected;
  if (strSelectedASCodeAgent != null && strSelectedASCodeAgent != "") {
    strAsCodeSelected = strSelectedASCodeAgent;
  } else {
    strAsCodeSelected = strASCode;
  }

  try {
    $http({
      url: URLInsure + "/GetInsuranceByClientNumberUnderAgent_New",
      method: "POST",
      data: {
        strClientNumber: EncryptDecryptData.Encrypt(strClntnumVw),
        strAgentNum: EncryptDecryptData.Encrypt(strAsCodeSelected)
      }
    }).then(
      function(response) {
        $scope.ListInsuranceNo = response.data.d;
        if (response.data.d != "") {
          fnGetInsureCardInfo(response.data.d[0].Chdrnum);
          $scope.HasData = "Y";
          $scope.DefaultListInsuranceNo = response.data.d[0].Chdrnum;
          fnGetPaymentLogByChdrnum($scope.DefaultListInsuranceNo);
          $scope.ddlSelectChdrnum = $scope.DefaultListInsuranceNo;
        } else {
          $scope.HasData = "N";
          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        }
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Fail");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  function fnGetPaymentLogByChdrnum(val) {
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    fnGetAllPaymentDocument(val);
  }

  $scope.ddlSelectChdrnum = ""; //Insure ID
  $scope.GetPaymentLogByChdrnum = function(val) {
    fnGetInsureCardInfo(val);
    fnGetPaymentLogByChdrnum(val);
  };

  function GetPOS001(ChdrnumVal) {
    var DocTPVal = "POS001";
    try {
      $http({
        url: URLInsure + "/GetPaymentDueDocument_New",
        method: "POST",
        data: { strChdrnum: EncryptDecryptData.Encrypt(ChdrnumVal.trim()) }
      }).then(
        function(response) {
          $scope.DocPOS001 = response.data.d;

          GetPOS014(ChdrnumVal);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function GetPOS014(ChdrnumVal) {
    var DocTPVal = "POS014";
    try {
      $http({
        url: URLInsure + "/GetPaymentDocument_New",
        method: "POST",
        data: {
          strChdrnum: EncryptDecryptData.Encrypt(ChdrnumVal.trim()),
          strDoctype: EncryptDecryptData.Encrypt(DocTPVal.trim())
        }
      }).then(
        function(response) {
          $scope.DocPOS014 = response.data.d;

          GetPOS037(ChdrnumVal);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function GetPOS037(ChdrnumVal) {
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    var DocTPVal = "POS037";
    try {
      $http({
        // url: URLInsure + "/GetBillPaymentAndDoc",
        url: URLInsure + "/GetPaymentHistory_New",
        method: "POST",
        data: { strChdrnum: EncryptDecryptData.Encrypt(ChdrnumVal) }
      }).then(
        function(response) {
          $scope.DocPOS037 = response.data.d;

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function fnGetAllPaymentDocument(ChdrnumVal) {
    GetPOS001(ChdrnumVal);
  }

  $scope.numberWithCommas = function(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  };

  $scope.ConvertJSONDateToString = function(DateVal) {
    var dateString = DateVal.substr(6);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var date = month + "/" + day + "/" + (parseInt(year) + 543).toString();
    return getFormattedDateTH(date);
  };

  $scope.ConvertYYYYMMDDDateToString = function(DateVal) {
    var sDate = DateVal.toString();
    var year = sDate.substr(0, 4);
    var month = sDate.substr(4, 2);
    var day = sDate.substr(6, 2);

    var date = month + "/" + day + "/" + (parseInt(year) + 543).toString();
    return getFormattedDateTH(date);
  };

  function getFormattedDateEN(input) {
    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = input.replace(pattern, function(match, p1, p2, p3) {
      var months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ];
      return (p2 < 10 ? "" + p2 : p2) + " " + months[p1 - 1] + " " + p3;
    });

    return result;
  }

  function getFormattedDateTH(input) {
    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = input.replace(pattern, function(match, p1, p2, p3) {
      var months = [
        "ม.ค.",
        "ก.พ.",
        "มี.ค.",
        "เม.ย.",
        "พ.ค.",
        "มิ.ย.",
        "ก.ค.",
        "ส.ค.",
        "ก.ย.",
        "ต.ค.",
        "พ.ย.",
        "ธ.ค."
      ];
      return (p2 < 10 ? "" + p2 : p2) + " " + months[p1 - 1] + " " + p3;
    });

    return result;
  }

  function ViewFile(URLFileVal) {
    if (ionic.Platform.platform() == "ios") {
      cordova.InAppBrowser.open(
        "data:application/pdf;base64," + URLFileVal,
        "_blank",
        "location=no"
      );
    } else {
      var today = new Date();
      var date =
        today.getFullYear() +
        "-" +
        (today.getMonth() + 1) +
        "-" +
        today.getDate();
      var time =
        today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
      var dateTime = date + "-" + time;

      var contentType = "application/pdf";
      var folderpath = cordova.file.externalRootDirectory;
      if (folderpath == null) folderpath = cordova.file.dataDirectory;
      var filename = $scope.ddlSelectChdrnum + "_" + dateTime + ".pdf";
      savebase64AsPDF(folderpath, filename, URLFileVal, contentType);
    }
  }

  // Start : Render PDF(base64) For Android
  function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || "";
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  function savebase64AsPDF(folderpath, filename, content, contentType) {
    // Convert the base64 string in a Blob
    var DataBlob = b64toBlob(content, contentType);
    window.resolveLocalFileSystemURL(folderpath, function(dir) {
      dir.getFile(filename, { create: true }, function(file) {
        file.createWriter(
          function(fileWriter) {
            fileWriter.write(DataBlob);
            var finalPath = folderpath + filename;
            //window.open(finalPath, '_system');
            cordova.plugins.fileOpener2.open(finalPath, "application/pdf");
          },
          function() {
            alert("err");
          }
        );
      });
    });
  }
  // End : Render PDF(base64) For Android

  function FTPDoc(ChdrnumVal, CertDocType, DocfullNM) {
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      $http({
        url: URLFtp + "/FTPPaymentDocument_New",
        method: "POST",
        data: {
          strChdrnum: EncryptDecryptData.Encrypt(ChdrnumVal),
          strDoctype: EncryptDecryptData.Encrypt(CertDocType),
          strDocPathFullNM: EncryptDecryptData.Encrypt(DocfullNM)
        }
      }).then(
        function(response) {
          $ionicLoading.hide();
          var strFPFN = response.data.d;
          ViewFile(strFPFN);
          // var strFPFN = response.data.d;
          // $scope.FPFN = strFPFN;
          // ViewFile(strFPFN);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            // alert("FTP Fail");
            alert(response.data.Message);
          }
          $ionicLoading.hide();
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
      $ionicLoading.hide();
    }
  }

  $scope.cancelSendEmail = () => {
    $scope.modal.hide();
  };

  $scope.confirmSendEmail = (DocType, DocfullNM) => {
    $scope.checkBTSendEmail = true;
    try {
      $http({
        url: URLFtp + "/FTPPaymentDocumentToEmail_New",
        method: "POST",
        data: {
          strAsCode: EncryptDecryptData.Encrypt(strASCode),
          strDoctype: EncryptDecryptData.Encrypt(DocType),
          strDocPathFullNM: EncryptDecryptData.Encrypt(DocfullNM)
        }
      }).then(
        function(response) {},
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert(response.data.Message);
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
    alert("ส่งอีเมลแล้ว");
    $scope.modal.hide();
  };

  $scope.OpenModalSendMail = (strDocType, strDocFullFileNM) => {
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      //debugger;
      $http({
        url: URLAG + "/GetAgentDetail",
        method: "POST",
        data: {
          strAsCode: EncryptDecryptData.Encrypt(strASCode)
        }
      }).then(
        function(response) {
          $ionicLoading.hide();
          // console.log(response.data.d[0]);

          $scope.AgentData = response.data.d[0];
          $scope.checkBTSendEmail = false;
          $scope.HasEmail = "Y";
          $scope.DocType = strDocType;
          $scope.DocfullNM = strDocFullFileNM;

          if ($scope.AgentData.Email == "" || !$scope.AgentData.Email) {
            $scope.HasEmail = "N";
          }

          $ionicModal
            .fromTemplateUrl("ModalSendMail_New.tmpl.html", {
              scope: $scope,
              animation: "slide-in-up"
            })
            .then(function(modal) {
              $scope.modal = modal;

              $scope.openModal = function() {
                $scope.modal.show();
              };

              $scope.closeModal = function() {
                $scope.modal.hide();
              };

              $scope.$on("$destroy", function() {
                //$scope.modal.remove();
              });

              $scope.openModal();
            });
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            // alert("FTP Fail");
            alert(response.data.Message);
          }
          $ionicLoading.hide();
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
      $ionicLoading.hide();
    }
  };

  $scope.GoToSetting = () => {
    $scope.modal.hide();

    // $ionicHistory.nextViewOptions({
    //   disableBack: true
    // });

    $state.go("app.ProfileSetting");
  };

  $scope.DownloadNoti = function(strDocFullFileNM) {
    var NotiDocType = "POS001";
    FTPDoc($scope.ddlSelectChdrnum, NotiDocType, strDocFullFileNM);
  };

  $scope.DownloadCert = function(strDocFullFileNM) {
    var CertDocType = "POS014";
    FTPDoc($scope.ddlSelectChdrnum, CertDocType, strDocFullFileNM);
  };

  $scope.DownloadBill = function(strDocFullFileNM) {
    var BillDocType = "POS037";
    FTPDoc($scope.ddlSelectChdrnum, BillDocType, strDocFullFileNM);
  };

  $scope.MailFrom_Modal = {};
  $scope.MailFrom = {
    text: "fill me in"
  };

  $scope.MailTo_Modal = {};
  $scope.MailTo = {
    text: "fill me in"
  };

  $scope.MailCC_Modal = {};
  $scope.MailCC = {
    text: "fill me in"
  };

  $scope.MailBCC_Modal = {};
  $scope.MailBCC = {
    text: "fill me in"
  };

  $scope.MailSubject_Modal = {};
  $scope.MailSubject = {
    text: "fill me in"
  };

  $scope.MailDetail_Modal = {};
  $scope.MailDetail = {
    text: "fill me in"
  };

  //$scope.MailTo_Modal.text = strEmail;
  $scope.MailTo_Modal.text = "";

  $scope.clearModalText = function() {
    $scope.MailFrom_Modal.text = "";
  };

  $scope.SendMailProcess = function(IsSendMail) {
    if (IsSendMail) {
      IsSendMail = false;
      $scope.MailFrom.text = $scope.MailFrom_Modal.text;
      $scope.MailTo.text = $scope.MailTo_Modal.text;
      $scope.MailCC.text = $scope.MailCC_Modal.text;
      $scope.MailBCC.text = $scope.MailBCC_Modal.text;

      $scope.MailSubject.text = $scope.MailSubject_Modal.text;
      $scope.MailDetail.text = $scope.MailDetail_Modal.text;

      if ($scope.MailTo.text == "") {
        alert("กรุณากรอกอีเมล์ผู้รับ");
        return;
      }

      if (
        ValidateMailTo($scope.MailTo.text) == false ||
        ValidateMailCC($scope.MailCC.text) == false ||
        ValidateMailBCC($scope.MailBCC.text) == false
      ) {
        alert("รูปแบบอีเมล์ไม่ถูกต้อง");
        return;
      }

      /*Send Mail*/
      try {
        if ($scope.MailCC.text == undefined) {
          $scope.MailCC.text = "";
        }
        if ($scope.MailBCC.text == undefined) {
          $scope.MailBCC.text = "";
        }
        if ($scope.MailSubject.text == undefined) {
          $scope.MailSubject.text = "";
        }
        if ($scope.MailDetail.text == undefined) {
          $scope.MailDetail.text = "";
        }

        $http({
          url: URLSendMail + "/SendFileToMail",
          method: "POST",
          data: {
            strMailFrom: varMailFrom,
            strMailTo: $scope.MailTo.text,
            strMailBcc: $scope.MailBCC.text,
            strMailCC: $scope.MailCC.text,
            strSubject: $scope.MailSubject.text,
            strBody: $scope.MailDetail.text,
            isBodyHtml: "true",
            strURLFile: $scope.FPFN
          }
        }).then(
          function(response) {
            alert("Send Mail Complete");
          },
          function(response) {
            if (response.status == 401) {
              TMLTHService.unauthorizedService();
            } else {
              alert("Send Mail Fail");
              alert(response.data.Message);
            }
          }
        );
      } catch (err) {
        alert("Error : " + err.message);
      }
    }

    $scope.MailFrom_Modal.text = "";
    $scope.MailTo_Modal.text = "";
    $scope.MailCC_Modal.text = "";
    $scope.MailBCC_Modal.text = "";

    $scope.MailSubject_Modal.text = "";
    $scope.MailDetail_Modal.text = "";

    $scope.MailTo_Modal.text = ""; //strEmail;

    $scope.closeModal();
  };

  //////////////////////////////////////

  function DownloadAndSendMail(DocType, strDocFullFileNM) {
    try {
      $http({
        url: URLFtp + "/FTPPaymentDocument",
        method: "POST",
        data: {
          strChdrnum: $scope.ddlSelectChdrnum,
          strDoctype: DocType,
          strDocPathFullNM: strDocFullFileNM
        }
      }).then(
        function(response) {
          var strFPFN = response.data.d;
          $scope.FPFN = strFPFN;

          var str1 = $scope.FPFN;
          str1 = str1.substr($scope.FPFN.lastIndexOf("/") + 1);
          $scope.FN = "";

          $scope.MailAttached = str1;

          $scope.AttachedFile = { File: $scope.FPFN };

          $scope.MailFromMsg = "";
          $scope.MailToMsg = "";
          $scope.MailCCMsg = "";
          $scope.MailBCCMsg = "";

          $ionicModal
            .fromTemplateUrl("ModalSendMail.tmpl.html", {
              scope: $scope,
              animation: "slide-in-up"
            })
            .then(function(modal) {
              $scope.modal = modal;

              $scope.openModal = function() {
                $scope.modal.show();
              };

              $scope.closeModal = function() {
                $scope.modal.hide();
              };

              $scope.$on("$destroy", function() {
                //$scope.modal.remove();
              });

              $scope.openModal();
            });
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("FTP Fail");
            alert(response.data.Message);
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.DownloadAndSendMailNoti = function(strDocFullFileNM) {
    var NotiDocType = "POS001";

    DownloadAndSendMail(NotiDocType, strDocFullFileNM);
  };

  $scope.DownloadAndSendMailCert = function(strDocFullFileNM) {
    var NotiDocType = "POS014";

    DownloadAndSendMail(NotiDocType, strDocFullFileNM);
  };

  $scope.DownloadAndSendMailBill = function(strDocFullFileNM) {
    var NotiDocType = "POS037";

    DownloadAndSendMail(NotiDocType, strDocFullFileNM);
  };

  /*Chk format Email*/
  $scope.ValidateMailFromFormat = function(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      $scope.MailFromMsg = "";
    } else {
      $scope.MailFromMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailToFormat = function(strEmail) {
    $scope.MailToMsg = "";
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    if (bl == true) {
      $scope.MailToMsg = "";
    } else {
      $scope.MailToMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailCCFormat = function(strEmail) {
    $scope.MailCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    if (bl == true) {
      $scope.MailCCMsg = "";
    } else {
      $scope.MailCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailBCCFormat = function(strEmail) {
    $scope.MailBCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    if (bl == true) {
      $scope.MailBCCMsg = "";
    } else {
      $scope.MailBCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  function ValidateEmailFormat(strEmail) {
    var email = strEmail;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      return false;
    } else {
      return true;
    }
  }

  function ValidateMailFrom(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      return true;
    } else {
      return false;
    }
  }

  function ValidateMailTo(strEmail) {
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }

  function ValidateMailCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }

  function ValidateMailBCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  /*End Chk format Email*/
});
