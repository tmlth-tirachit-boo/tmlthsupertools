var ActionVal = ActRegister;

CPApp.controller("RegisterCtrl", function(
  $scope,
  $http,
  $state,
  $timeout,
  $ionicLoading,
  TMLTHService
) {
  // console.log("RegisterCtrl");

  $scope.disabled = false;
  //alert(ActionVal);

  $scope.CanSubmit = true;
  $scope.RegIDCard = "";
  $scope.Chdrnum = "";
  $scope.Name_ins = "";
  $scope.Lastname_ins = "";
  $scope.Mobile_no = "";
  $scope.Phone = "";
  $scope.Email = "";
  $scope.Userid = "";
  $scope.Pwd = "";
  $scope.ConfirmPwd = "";

  $scope.DupIDCarsMsg = "";
  $scope.DupUserIDMsg = "";
  $scope.InvalidEmailFormatMsg = "";

  $scope.Clntnum = "";

  $scope.PWDMsg = "";

  /*Chk Format ID Card*/
  function checkID(id) {
    if (id.length != 13) return false;
    for (i = 0, sum = 0; i < 12; i++)
      sum += parseFloat(id.charAt(i)) * (13 - i);
    if ((11 - (sum % 11)) % 10 != parseFloat(id.charAt(12))) return false;
    return true;
  }
  function CheckFormatIDCard(IDCardVal) {
    if (!checkID(IDCardVal)) return false;
    else return true;
  }
  /*End Chk Format ID Card*/

  /*GetOnecUserForRegister*/
  function GetOnecUserForRegister(valIDCard) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    try {
      $http({
        url: URLRegister + "/GetOnecUserForRegister",
        method: "POST",
        data: {
          strIDCard: valIDCard,
          strChdrnum: "",
          strLgivname_ins: "",
          strLsurname_ins: ""
        }
      }).then(
        function(response) {
          $scope.Chdrnum = response.data.d.Chdrnum;
          $scope.Name_ins = response.data.d.Lgivname_ins;
          $scope.Lastname_ins = response.data.d.Lsurname_ins;
          $scope.Mobile_no = response.data.d.Cltphone02_ins;
          $scope.Phone = response.data.d.Cltphone01_ins;

          $scope.Clntnum = response.data.d.Clntnum_ins;
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }

    $timeout(function() {
      $ionicLoading.hide();
    }, 2000);
  }
  /*End GetOnecUserForRegister*/

  /*Check Dupplicate IDCard*/
  $scope.IsDupIDCard = function(valIDCard) {
    if (valIDCard == undefined) {
      return;
    }

    /*IDCard*/
    if (valIDCard.length != 13) {
      //alert('หมายเลขบัตรประชาชน ต้องมี 13 หลัก ');
      $scope.DupIDCarsMsg = "หมายเลขบัตรประชาชน ต้องมี 13 หลัก";
      $scope.CanSubmit = false;
      return false;
    } else {
      if (CheckFormatIDCard(valIDCard) == false) {
        //alert('รูปแบบของหมายเลขบัตรไม่ถูกต้อง');
        $scope.DupIDCarsMsg = "รูปแบบของหมายเลขบัตรไม่ถูกต้อง";
        $scope.CanSubmit = false;
        return false;
      } else {
        $scope.DupIDCarsMsg = "";
        $scope.CanSubmit = true;
      }
    }

    try {
      $http({
        url: URLRegister + "/IsDupIDCard",
        method: "POST",
        data: { strIDCard: valIDCard }
      }).then(
        function(response) {
          if (response.data.d == false) {
            $scope.DupIDCarsMsg = "";
            $scope.CanSubmit = true;
          } else {
            $scope.DupIDCarsMsg = "หมายเลขบัตรประชาชนนี้ถูกใช้งานแล้ว";
            $scope.CanSubmit = false;
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  };
  /*End Check Dupplicate IDCard*/

  /*Check Dupplicate UserID*/
  $scope.IsDupUserID = function(valUserId) {
    //alert(valUserId);
    if (valUserId == undefined) {
      return;
    }

    try {
      $http({
        url: URLRegister + "/IsDupUserID",
        method: "POST",
        data: { strUserID: valUserId }
      }).then(
        function(response) {
          if (response.data.d == false) {
            $scope.DupUserIDMsg = "";
            $scope.CanSubmit = true;
          } else {
            $scope.DupUserIDMsg = "ชื่อผู้ใช้งานนี้ถูกใช้งานแล้ว";
            $scope.CanSubmit = false;
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  };
  /*End Check Dupplicate UserID*/

  // function IsAlphaNumericWithMessage(obj, message) {
  // function IsAlphaNumericWithMessage(obj) {
  //             var text = obj;
  //             var letter = /[a-zA-Z]/;
  //             var number = /[0-9]/;
  //             var minlength = /^[0-9a-zA-Z]{6,}/;
  //             var validAlphaNumeric = number.test(text) && letter.test(text);
  //             var validMinLength = minlength.test(text);
  //
  //             if (!validAlphaNumeric || !validMinLength) {
  //                 //alert(message);
  //                 obj = '';
  // return false;
  //             }else{return true;}
  //         }

  function IsAlphaNumericWithMessage(obj) {
    var text = obj;
    var letter = /[a-zA-Z]/;
    var number = /[0-9]/;
    var minlength = /^[0-9a-zA-Z]{6,20}$/;
    var validAlphaNumeric = number.test(text) && letter.test(text);
    var validMinLength = minlength.test(text);

    if (!validAlphaNumeric || !validMinLength) {
      //alert(message);
      //obj.value = '';
      obj = "";
      return false;
    } else {
      return true;
    }
  }

  /*Check Username*/
  $scope.IsValidUsername = function(valUserId) {
    //alert(valUserId);
    if (valUserId == undefined) {
      return;
    }

    if (IsAlphaNumericWithMessage(valUserId)) {
      //check dup

      try {
        $http({
          url: URLRegister + "/IsDupUserID",
          method: "POST",
          data: { strUserID: valUserId }
        }).then(
          function(response) {
            if (response.data.d == false) {
              $scope.DupUserIDMsg = "";
              $scope.CanSubmit = true;
            } else {
              $scope.DupUserIDMsg = "รหัสผู้ใช้งานนี้ถูกใช้งานแล้ว";
              $scope.CanSubmit = false;
            }
          },
          function(response) {
            if (response.status == 401) {
              TMLTHService.unauthorizedService();
            } else {
              alert("Error");
            }
          }
        );
      } catch (err) {
        alert("Error : " + err.message);
      }
    } else {
      //$scope.DupUserIDMsg='ชื่อผู้ใช้งานนี้ไม่ถูกต้องตามหลักการตั้งชื่อ';
      $scope.DupUserIDMsg =
        "รหัสผู้ใช้งาน ประกอบด้วยตัวอักษรภาษาอังกฤษตัวใหญ่/ตัวเล็ก/ตัวเลข อย่างน้อย 6-20 ตัวอักษร";
      $scope.CanSubmit = false;
    }
  };
  /*End Check Username*/

  /*Check PWD Policy*/
  $scope.IsValidPWD = function(valPWD) {
    if (valPWD == undefined) {
      return;
    }

    //var reg= /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()-_+=<>.?:;{}|/,])[a-zA-Z0-9!@#$%^&*()-_+=<>.?:;{}|/,]{8,20}$/;
    var reg = /^(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{8,20}$/;
    var testV = /[a-zA-Z0-9]{8,20}$/;
    if (valPWD == "") {
      $scope.PWDMsg = "กรุณาระบุรหัสผ่าน";
      $scope.CanSubmit = false;
    } else {
      if (valPWD == "" || valPWD.length < 8 || !reg.test(valPWD)) {
        //$scope.PWDMsg='รูปแบบของรหัสผ่านไม่ถูกต้อง';
        $scope.PWDMsg =
          "รหัสผ่านผู้ใช้งาน ประกอบด้วยตัวอักษรภาษาอังกฤษตัวใหญ่/ตัวเล็ก/ตัวเลข/อักขระพิเศษ อย่างน้อย 8-20 ตัวอักษร";
        $scope.CanSubmit = false;
        // $("#Password1_validate").show();
        // $("#<%=Password1.ClientID%>").addClass("input-error");
        // $("#<%=Password1.ClientID%>").removeClass("input-success");
      } else {
        $scope.PWDMsg = "";
        $scope.CanSubmit = true;
      }
    }
  };

  /*End Check PWD Policy*/

  /*Check Confirm PWD Policy*/
  $scope.IsValidConfirmPWD = function(valPWD) {
    if (valPWD == undefined) {
      return;
    }

    //var reg= /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()-_+=<>.?:;{}|/,])[a-zA-Z0-9!@#$%^&*()-_+=<>.?:;{}|/,]{8,20}$/;
    var reg = /^(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{8,20}$/;
    var testV = /[a-zA-Z0-9]{8,20}$/;
    if (valPWD == "") {
      $scope.ConfirmPWDMsg = "กรุณาระบุรหัสผ่าน";
      $scope.CanSubmit = false;
    } else {
      if (valPWD == "" || valPWD.length < 8 || !reg.test(valPWD)) {
        //$scope.PWDMsg='รูปแบบของรหัสผ่านไม่ถูกต้อง';
        $scope.ConfirmPWDMsg =
          "รหัสผ่านผู้ใช้งาน ประกอบด้วยตัวอักษรภาษาอังกฤษตัวใหญ่/ตัวเล็ก/ตัวเลข/อักขระพิเศษ อย่างน้อย 8-20 ตัวอักษร";
        $scope.CanSubmit = false;
        // $("#Password1_validate").show();
        // $("#<%=Password1.ClientID%>").addClass("input-error");
        // $("#<%=Password1.ClientID%>").removeClass("input-success");
      } else {
        $scope.ConfirmPWDMsg = "";
        $scope.CanSubmit = true;
      }
    }
  };

  /*End Check Confirm PWD Policy*/

  /*Check Phone Format*/
  $scope.IsPhoneNo = function(ValPhone) {
    var regExp = /^0[0-9]{8,9}$/i;

    if (ValPhone == "" || ValPhone.length != 10 || !regExp.test(ValPhone)) {
      $scope.InvalidPhoneFormatMsg = "รูปแบบของเบอร์โทรศัพท์ไม่ถูกต้อง";
      $scope.CanSubmit = false;
    } else {
      $scope.InvalidPhoneFormatMsg = "";
      $scope.CanSubmit = true;
    }
  };

  function IsPhoneFormat(ValPhone) {
    var regExp = /^0[0-9]{8,9}$/i;

    if (ValPhone == "" || ValPhone.length != 10 || !regExp.test(ValPhone)) {
      return false;
    } else {
      return true;
    }
  }
  /*End Check Phone Format*/

  /*Check Mobile is Avilable*/
  // $scope.IsMobileAvilable = function(valIDCard,valMobileNo) {
  // //alert(valIDCard);
  // if (valIDCard == undefined)
  // {
  // $scope.InvalidPhoneFormatMsg = 'กรุณาระบุหมายเลขบัตรประชาชน';
  // $scope.CanSubmit = false;
  // return;
  // }

  // if(IsPhoneFormat(valMobileNo)==false)
  // {
  // $scope.InvalidPhoneFormatMsg='รูปแบบของเบอร์โทรศัพท์ไม่ถูกต้อง';
  // $scope.CanSubmit=false;
  // return;
  // }else{

  // $scope.InvalidPhoneFormatMsg='';
  // $scope.CanSubmit=true;

  // try {
  // $http({
  // url: URLRegister + "/IsMobileAvilable",
  // method: "POST",
  // data: { 'strIDCard': valIDCard ,'strMobileNos': valMobileNo }
  // })
  // .then(function(response) {

  // if(response.data.d == true){
  // $scope.InvalidPhoneFormatMsg='';
  // $scope.CanSubmit = true;

  // }else{
  // $scope.InvalidPhoneFormatMsg='เบอร์โทรศัพท์มือถือดังกล่าวไม่มามารถนำมาลงทะเบียนได้';
  // $scope.CanSubmit = false;
  // }

  // },
  // function(response) { // optional
  // // failed
  // alert('Fail');
  // //debugger;
  // });

  // } catch (err) {
  // alert('Error : ' + err.message);
  // }

  // }

  // }
  // /*End Check Dupplicate IDCard*/

  // /*Chk format Email*/
  $scope.ValidateEmailFormat = function checkEmail(strEmail) {
    var email = strEmail;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      //alert('Please provide a valid email address');
      //email.focus;
      $scope.InvalidEmailFormatMsg = "อีเมล์ ไม่ถูกรูปแบบ";
      $scope.CanSubmit = false;
      return false;
    } else {
      $scope.InvalidEmailFormatMsg = "";
      $scope.CanSubmit = true;
      return true;
    }
  };
  /*End Chk format Email*/

  $scope.Register = function(
    IDCardVal,
    ChdrnumVal,
    Name_insVal,
    Lastname_insVal,
    Mobile_noVal,
    PhoneVal,
    EmailVal,
    UseridVal,
    PwdVal,
    ConfirmPwdVal
  ) {
    $scope.disabled = true;
    $timeout(function() {
      $scope.disabled = false;
    }, 10000);

    if ($scope.CanSubmit == false) {
      alert("ไม่สามารถลงทะเบียนได้ กรุณาตรวจสอบข้อมูลอีกครั้ง");
      return;
    }

    if (
      ValidateReg(
        IDCardVal,
        ChdrnumVal,
        Name_insVal,
        Lastname_insVal,
        Mobile_noVal,
        PhoneVal,
        EmailVal,
        UseridVal,
        PwdVal,
        ConfirmPwdVal
      ) == true
    ) {
      //alert(Mobile_noVal);

      if (
        IsUserForRegister(
          IDCardVal,
          ChdrnumVal,
          Name_insVal,
          Lastname_insVal,
          Mobile_noVal,
          PhoneVal,
          EmailVal,
          UseridVal,
          PwdVal,
          ActionVal
        ) == false
      ) {
        return;
      }
    } else {
      alert("ไม่สามารถลงทะเบียนได้ กรุณาตรวจสอบข้อมูลอีกครั้ง");
      return;
    }
  };

  function ValidateReg(
    IDCardVal,
    ChdrnumVal,
    Name_insVal,
    Lastname_insVal,
    Mobile_noVal,
    PhoneVal,
    EmailVal,
    UseridVal,
    PwdVal,
    ConfirmPwdVal
  ) {
    var blResult = true;

    //Required Field//
    /*if((IDCardVal == '') || (ChdrnumVal == '') || (Name_insVal == '') ||(Lastname_insVal == '') ||(Mobile_noVal == '') ||(EmailVal == '') ||(UseridVal == '') ||(PwdVal == '') ||(ConfirmPwdVal == ''))*/
    if (
      IDCardVal == "" ||
      Name_insVal == "" ||
      Lastname_insVal == "" ||
      Mobile_noVal == "" ||
      EmailVal == "" ||
      UseridVal == "" ||
      PwdVal == "" ||
      ConfirmPwdVal == ""
    ) {
      alert("กรุณากรอกข้อมูลให้ครบ");
      blResult = false;
      return false;
    }

    //IDCard//
    if (IDCardVal.length != 13) {
      //alert('หมายเลขบัตรประชาชน ต้องมี 13 หลัก ');
      blResult = false;
      return false;
    } else {
      if (CheckFormatIDCard(IDCardVal) == false) {
        //alert('รูปแบบของหมายเลขบัตรไม่ถูกต้อง');
        blResult = false;
        return false;
      }
    }

    var email = EmailVal;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      blResult = false;
      return false;
    }

    //PWD Policy//
    //End PWD Policy//

    //PWD Match//
    if (PwdVal != ConfirmPwdVal) {
      alert("กรุณาระบุรหัสผ่านให้ตรงกัน");
      blResult = false;
      return false;
    }

    if (blResult != false) {
      //debugger;
      //alert('555')
      return true;
    }
  }

  function IsUserForRegister(
    IDCardVal,
    ChdrnumVal,
    Name_insVal,
    Lastname_insVal,
    Mobile_noVal,
    PhoneVal,
    EmailVal,
    UseridVal,
    PwdVal,
    ActionVal
  ) {
    try {
      $http({
        url: URLRegister + "/GetUserForRegister",
        method: "POST",
        data: {
          strIDCard: IDCardVal,
          strChdrnum: ChdrnumVal,
          strLgivname_ins: Name_insVal,
          strLsurname_ins: Lastname_insVal,
          strMobileNo: Mobile_noVal
        }
      }).then(
        function(response) {
          var objResult = response.data.d[0];
          debugger;

          if (objResult.Massage != "OK") {
            alert(objResult.Massage);
            return false;
          } else {
            /*$state.go('OTP', {pIDCard: IDCardVal, pClntnum:objResult.Clntnum_ins, pChdrnum:ChdrnumVal, pName_ins:Name_insVal, pLastname_ins:Lastname_insVal, pMobile_no:Mobile_noVal, pPhone:PhoneVal, pEmail:EmailVal, pUserid:UseridVal, pPwd:PwdVal, pAction:ActionVal});*/

            $state.go("OTP", {
              pIDCard: IDCardVal,
              pClntnum: objResult.Clntnum_ins,
              pChdrnum: objResult.Chdrnum,
              pName_ins: Name_insVal,
              pLastname_ins: Lastname_insVal,
              pMobile_no: Mobile_noVal,
              pPhone: PhoneVal,
              pEmail: EmailVal,
              pUserid: UseridVal,
              pPwd: PwdVal,
              pAction: ActionVal
            });
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
          return false;
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
      return false;
    }
  }

  function Test() {
    alert("Test");
  }

  $scope.GoToLogin = function() {
    $state.go("Login", {}, { reload: true });
  };

  //  $scope.TmpOTP=function()
  // {
  //   //alert('click');
  //   //$state.go('OTP', {pIDCard: '555'}, { reload: true });

  //     $state.go('OTP', {pIDCard: '8899',pChdrnum:'5566',pName_ins:'JK',pLastname_ins:'KPS',pMobile_no:'0844259888',pPhone:'-',pEmail:'JK@gmail.com',pUserid:'JKuser',pPwd:'1234',pAction:'registration'});
  //     //$state.go('OTP', {pIDCard: '8899',pChdrnum:'5566'});
  // }

  // $scope.Regist = function() {
  //   //console.log('Regist', user);
  //   $state.go('Login');
  // };

  // $scope.Regist = function(user) {
  //   console.log('Regist', user);
  //   $state.go('Login');
  // };

  // $scope.data={"ImageURI" : "Select Image"};

  // function UploadPicture(imageURI){
  //   $scope.data.imageURI = imageURI;
  //   alert($scope.data.imageURI);
  // }

  // $scope.ShowPictures = function(){
  //   navigator.camera.getPicture(UploadPicture,function(message){
  //     alert('get picture failed');
  //   },{
  //     quality:50,
  //     destinationType:navigator.camera.destinationType.FILE_URI,
  //     sourceType:navigator.camera.PictureSourceType.PHOTOLIBRARY
  //   });

  // };
});
