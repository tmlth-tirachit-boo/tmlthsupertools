CPApp.controller("AgentAuthorizationCtrl", function(
  $scope,
  $state,
  $http,
  $timeout,
  $ionicLoading,
  TMLTHService,
  EncryptDecryptData,
  JWTService,
  $rootScope
) {
  $rootScope.showPDB = true;
  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 10
  });

  $timeout(function() {
    $ionicLoading.hide();
  }, 1000);

  $scope.disabled = false;

  $scope.GoToAGWEB = function() {
    window.open(
      encodeURI("https://www.tokiomarinelife.co.th/agweb/"),
      "_blank",
      "location=yes"
    );
  };

  $scope.ErrMsg = "";
  $scope.Msg =
    "<div>สามารถใช้ชื่อผู้ใช้งาน/รหัสผ่านเดียวกับ</div><div>เว็บไซต์บริษัท (ช่องทางตัวแทน)</div>";

  SplashScreen();

  function SplashScreen() {
    JWTService.Del();
    JWTService.DelBasic();
    try {
      $http({
        url: URLCommon + "/SplashScreen",
        method: "POST",
        data: { key: AppID }
      }).then(
        function(response) {
          // TokenManager.SetBasic(response.data.d);
          JWTService.SetBasic(response.data.d);
          $http.defaults.headers.common.Authorization =
            "Bearer" +
            " " +
            JWTService.Get() +
            ",Basic" +
            " " +
            JWTService.GetBasic();

          checkCurrent();
        },
        function(response) {
          // optional
          // alert("Fail SplashScreen");
        }
      );
    } catch (err) {
      alert("Error SplashScreen: " + err.message);
    }
  }
  function checkCurrent() {
    try {
      $http({
        url: URLCommon + "/NeedUpdateVersion",
        method: "POST",
        data: { strAppID: AppID, strInputVer: AppVer }
      }).then(
        function(response) {
          //alert('Success');

          $scope.objNeedUpdate = response.data.d;

          if ($scope.objNeedUpdate == true) {
            $state.go("Message");
          }
        },
        function(response) {
          // optional
          // failed
          // alert("Fail NeedUpdateVersion");
          //debugger;
        }
      );
    } catch (err) {
      alert("Error NeedUpdateVersion: " + err.message);
    }
  }

  $scope.LoginUserId = "";
  $scope.LoginUserPwd = "";

  $scope.ddlSelectProvinceValue = "";
  $scope.LogIn = function(userid, pwd) {
    if (userid != "" && pwd != "") {
      fnAgentLogin(userid, pwd);
    } else {
      $scope.ErrMsg = "กรุณากรอกข้อมูลให้ครบ";
    }
  };

  $scope.Forgot = function(user) {
    $scope.LoginUserId = "";
    $scope.LoginUserPwd = "";
    $state.go("Login", {}, { reload: true });
  };

  $scope.Logout = function() {
    alert("Bye");
  };

  function fnAgentLogin(userid, pwd) {
    var Uid = userid;
    var Pass = pwd.trim();

    try {
      $http({
        url: URLAG + "/AgentLogin_New",
        method: "POST",
        data: {
          strUsrID: EncryptDecryptData.Encrypt(Uid),
          strPWD: EncryptDecryptData.Encrypt(Pass)
          // strAppID: AppID,
          // strAppVersion: AppVer
        }
      }).then(
        function(response) {
          // console.log(response);
          if (response.data.d == "") {
            strIDCard = "";
            strClntnum = "";
            strChdrnum = "";
            strQuestion = "";
            strAnswer = "";
            strPassword = "";
            strUserid = "";
            strName_ins = "";
            strLastname_ins = "";
            strMobile_no = "";
            dtMobile_dt = "";
            strPrev_mobile = "";
            strEmail = "";
            dtEmail_dt = "";
            strPrev_email = "";
            strAgreement = "";
            dtUpddate = "";
            dtRegister_dt = "";
            strPhone = "";
            strPrev_phone = "";
            dtPhone_dt = "";
            strReset_password = "";
            guidReset_code = "";
            dtReset_date = "";
            strSend_life_asia = "";
            dtSend_dt = "";
            strCltdob_ins = "";
            /*Agent*/
            strIsAgent = "Y";
            strAgentLv = "";
            strASCode = "";
            strASName = "";
            strASSName = "";
            strASTel = "";
            strASInfo = "";
            strASStatus = "";
            strPostnco = "";
            strUnitco = "";
            strUntStatus = "";
            strASPost = "";
            strLevelCo = "";
            strALCode = "";
            strALName = "";
            strALSName = "";
            strALTel = "";
            strALInfo = "";
            strALStatus = "";
            strALPost = "";
            strGRPCode = "";
            strGRPStatus = "";
            strAECode = "";
            strAEName = "";
            strAESName = "";
            strAETel = "";
            strAEInfo = "";
            strAEPost = "";
            strAreaCo = "";
            strAEStatus = "";
            strUpdated_dt = "";
            strMktchan = "";
            strASJoinDate = "";
            strALJoinDate = "";
            strAEJoinDate = "";
            strChaASCode = "";
            strChaPassword = "";
            strChaUser = "";
            strNLevel = "";
            strRegDte = "";
            strRegCo = "";
            strForgCo = "";
            strMktChan_TUserPass = "";
            strUser_Type = "";
            strAgent_Statement = "";

            // Keep Access Log
            fnSaveApplicationAccessLog(Uid, "F");
            $scope.ErrMsg = "ชื่อหรือรหัสผ่านไม่ถูกต้อง";
          } else if (response.data.d[0].IsUserLock == "Y") {
            $scope.ErrMsg = response.data.d[0].IsUserLock_Message;
          } else {
            // Keep Access Log
            fnSaveApplicationAccessLog(Uid, "P");

            //alert('Data');
            /*Agent*/
            strIsAgent = "Y";
            strAgentLv = response.data.d[0].LevelCo;
            strASCode = response.data.d[0].Ascode;
            strASName = response.data.d[0].Asname;
            strASSName = response.data.d[0].Assname;
            strASTel = response.data.d[0].Astel;
            strASInfo = response.data.d[0].Asinfo;
            strASStatus = response.data.d[0].Asstatus;
            strPostnco = response.data.d[0].Postnco;
            strUnitco = response.data.d[0].Unitco;
            strUntStatus = response.data.d[0].Untstatus;
            strASPost = response.data.d[0].Aspost;
            strLevelCo = response.data.d[0].Levelco;
            strALCode = response.data.d[0].Alcode;
            strALName = response.data.d[0].Alname;
            strALSName = response.data.d[0].Alsname;
            strALTel = response.data.d[0].Altel;
            strALInfo = response.data.d[0].Alinfo;
            strALStatus = response.data.d[0].Alstatus;
            strALPost = response.data.d[0].Alpost;
            strGRPCode = response.data.d[0].Grpcode;
            strGRPStatus = response.data.d[0].Grpstatus;
            strAECode = response.data.d[0].Aecode;
            strAEName = response.data.d[0].Aename;
            strAESName = response.data.d[0].Aesname;
            strAETel = response.data.d[0].Aetel;
            strAEInfo = response.data.d[0].Aeinfo;
            strAEPost = response.data.d[0].Aepost;
            strAreaCo = response.data.d[0].Areaco;
            strAEStatus = response.data.d[0].Aestatus;
            strUpdated_dt = response.data.d[0].Updated_dt;
            strMktchan = response.data.d[0].Mktchan;
            strASJoinDate = response.data.d[0].Asjoindate;
            strALJoinDate = response.data.d[0].Aljoindate;
            strAEJoinDate = response.data.d[0].Aejoindate;
            strChaASCode = response.data.d[0].Chaascode;
            strChaPassword = response.data.d[0].Chapassword;
            strChaUser = response.data.d[0].Chapassword;
            strNLevel = response.data.d[0].Nlevel;
            strRegDte = response.data.d[0].Regdte;
            strRegCo = response.data.d[0].Regco;
            strForgCo = response.data.d[0].Forgco;
            strMktChan_TUserPass = response.data.d[0].Mktchan_tuserpass;
            strUser_Type = response.data.d[0].User_type;
            strAgent_Statement = response.data.d[0].Agent_statement;
            strHospitalProvSelected = "";
            strSelectedASCodeAgent = "";

            // SET TOKEN
            // TokenManager.Set(response.data.d[0].Token);
            // $http.defaults.headers.common.Authorization =
            //   "Bearer" +
            //   " " +
            //   TokenManager.Get() +
            //   ",Basic" +
            //   " " +
            //   TokenManager.GetBasic();
            JWTService.Set(response.data.d[0].Token);
            $http.defaults.headers.common.Authorization =
              "Bearer" +
              " " +
              JWTService.Get() +
              ",Basic" +
              " " +
              JWTService.GetBasic();

            $state.go("app.Home");
          }
        },
        function(response) {
          $scope.ErrMsg = response.message;
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function fnSaveApplicationAccessLog(userID, loginStatus) {
    try {
      $http({
        url: URLCommon + "/ApplicationAccessLog",
        method: "POST",
        data: {
          strAppID: AppID,
          strAppVersion: AppVer,
          strUserID: userID,
          strLoginStatus: loginStatus
        }
      }).then(
        function(response) {},
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error GetAppVersionService: " + err.message);
    }
  }
});
