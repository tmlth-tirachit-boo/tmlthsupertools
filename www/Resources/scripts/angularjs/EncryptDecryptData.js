CPApp.factory("EncryptDecryptData", function() {
  // var key = CryptoJS.enc.Utf8.parse("8080808080808080");
  // var iv = CryptoJS.enc.Utf8.parse("8080808080808080");
  var key = CryptoJS.enc.Utf8.parse("SUPERTOOLS123456789A9B9C9D9E9F9G");
  var iv = CryptoJS.enc.Utf8.parse("SUPERTOOLS123456");

  return {
    Encrypt: function(input) {
      try {
        let output = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(input), key, {
          keySize: 128 / 8,
          // keySize: 256 / 8,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
        });
        return output.toString();
      } catch (err) {
        alert("Error : " + err.message);
      }
    }
  };
});
