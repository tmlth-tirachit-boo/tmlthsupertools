CPApp.controller("NotificationCtrl", function($scope, $stateParams) {
  // console.log('NotificationCtrl');

  //Param To News Detail
  //$scope.lat = $stateParams.lat;
  //  $scope.lng = $stateParams.lng;
  //alert(IDCard);
  //alert(USRInfo.strIDCard);

  //alert(strIDCard);
  //alert(strName_ins);

  $scope.ListNotification = [
    {
      id: 3,
      NotificationTitle: "ข้อความจากระบบ3",
      NotificationMsg: "ข้อความทดสอบจากระบบ ส่งมาเพื่อทดสอบ ในวันที่ 20160421",
      NotificationBy: "System",
      NotificationDate: "2016-04-21",
      NotificationStatus: "New",
      NotificationTo: "USR"
    },
    {
      id: 2,
      NotificationTitle: "ข้อความจากระบบ2",
      NotificationMsg: "ข้อความทดสอบจากระบบ ส่งมาเพื่อทดสอบ ในวันที่ 20160420",
      NotificationBy: "System",
      NotificationDate: "2016-04-20",
      NotificationStatus: "New",
      NotificationTo: "USR"
    },
    {
      id: 1,
      NotificationTitle: "ข้อความจากระบบ1",
      NotificationMsg: "ข้อความทดสอบจากระบบ ส่งมาเพื่อทดสอบ ในวันที่ 20160401",
      NotificationBy: "System",
      NotificationDate: "2016-04-01",
      NotificationStatus: "Open",
      NotificationTo: "USR"
    }
  ];

  $scope.CountNotification = "3";
  $scope.CountNewNotification = "2";
});
