CPApp.controller("CreateNewPWDCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  TMLTHService
) {
  // console.log("CreateNewPWDCtrl");

  //Param To News Detail
  $scope.id = $stateParams.id;
  $scope.pIDCard = $stateParams.pIDCard;
  $scope.pOTP = $stateParams.pOTP;
  $scope.pOTP_ID = $stateParams.pOTP_ID;

  $scope.NewPwd = "";
  $scope.ConfirmPwd = "";
  $scope.CanSubmit = true;

  $scope.Submit = function(NewPwd, ConfirmPwd) {
    if (NewPwd.trim() != ConfirmPwd.trim()) {
      alert("กรุณาระบุรหัสผ่านให้ตรงกัน");
      return;
    }

    if ($scope.CanSubmit == false) {
      return;
    }

    try {
      $http({
        url: URLAuth + "/UpdateUsrPWD",
        method: "POST",
        data: {
          strIDCard: $scope.pIDCard,
          strPWD: NewPwd,
          strOTPID: $scope.pOTP_ID
        }
      }).then(
        function(response) {
          //debugger;
          if (response.data.d == "VALID") {
            alert("บันทึกเรียบร้อย");
            $state.go("Login", {}, { reload: true });
          } else {
            alert("บันทึกข้อมูลล้มเหลว");
            return;
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  };

  /*Check PWD Policy*/
  $scope.IsValidPWD = function(valPWD) {
    //alert(valPWD);
    if (valPWD == undefined) {
      $scope.CanSubmit = false;
      return;
    }

    //var reg= /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()-_+=<>.?:;{}|/,])[a-zA-Z0-9!@#$%^&*()-_+=<>.?:;{}|/,]{8,20}$/;
    var reg = /^(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{8,20}$/;
    var testV = /[a-zA-Z0-9]{8,20}$/;
    if (valPWD == "") {
      //alert('yyy');
      $scope.PWDMsg = "กรุณาระบุรหัสผ่าน";
      $scope.CanSubmit = false;
    } else {
      if (valPWD == "" || valPWD.length < 8 || !reg.test(valPWD)) {
        //$scope.PWDMsg='รูปแบบของรหัสผ่านไม่ถูกต้อง';
        $scope.PWDMsg =
          "รหัสผ่านผู้ใช้งาน ประกอบด้วยตัวอักษรภาษาอังกฤษตัวใหญ่/ตัวเล็ก/ตัวเลข/อักขระพิเศษ อย่างน้อย 8-20 ตัวอักษร";
        $scope.CanSubmit = false;
      } else {
        $scope.PWDMsg = "";
        $scope.CanSubmit = true;
      }
    }
  };

  /*End Check PWD Policy*/

  /*Check Confirm PWD Policy*/
  $scope.IsValidConfirmPWD = function(valPWD) {
    if (valPWD == undefined) {
      return;
    }

    //var reg= /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()-_+=<>.?:;{}|/,])[a-zA-Z0-9!@#$%^&*()-_+=<>.?:;{}|/,]{8,20}$/;
    var reg = /^(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{8,20}$/;
    var testV = /[a-zA-Z0-9]{8,20}$/;
    if (valPWD == "") {
      $scope.ConfirmPWDMsg = "กรุณาระบุรหัสผ่าน";
      $scope.CanSubmit = false;
    } else {
      if (valPWD == "" || valPWD.length < 8 || !reg.test(valPWD)) {
        //$scope.PWDMsg='รูปแบบของรหัสผ่านไม่ถูกต้อง';
        $scope.ConfirmPWDMsg =
          "รหัสผ่านผู้ใช้งาน ประกอบด้วยตัวอักษรภาษาอังกฤษตัวใหญ่/ตัวเล็ก/ตัวเลข/อักขระพิเศษ อย่างน้อย 8-20 ตัวอักษร";
        $scope.CanSubmit = false;
      } else {
        $scope.ConfirmPWDMsg = "";
        $scope.CanSubmit = true;
      }
    }
  };

  /*End Check Confirm PWD Policy*/
});
