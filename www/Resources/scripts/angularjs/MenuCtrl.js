CPApp.controller("MenuCtrl", function(
  $scope,
  $timeout,
  $ionicLoading,
  $ionicHistory,
  $location,
  $state,
  $http,
  EncryptDecryptData,
  JWTService
) {
  // console.log("MenuCtrl");

  $scope.CountNotificationAtMenu = "2";

  $scope.strLvCo = strLevelCo;

  // ================ Add by Tirachit B. @20170630 ================= //
  $scope.appVersion = AppVer;
  // ================ End add by Tirachit B. @20170630 ============= //

  $scope.GoToCustomerList = function() {
    $state.go("app.CustomerList", { ascode: strASCode });
  };

  $scope.GoToAgentList = function() {
    $state.go("app.AgentList", { unitco: strUnitco });
  };

  $scope.GoToUnitList = function() {
    $state.go("app.UnitList", { ascode: strASCode });
  };

  $scope.GoToFailReport = function(pType) {
    strPaymentType = pType;

    if ($scope.strLvCo === "61") {
      $state.go("app.FailRptCustomerList", { ascode: strASCode });
    } else if ($scope.strLvCo === "62") {
      $state.go("app.FailRptAgentList", { unitco: strUnitco });
    } else if ($scope.strLvCo === "63") {
      $state.go("app.FailRptUnitList", { ascode: strASCode });
    }
  };

  getBrochureLink();

  $scope.openBrochureLink = urlPBC => {
    if (ionic.Platform.platform() == "ios") {
      window.open(urlPBC, "_system", "location=yes,enableviewportscale=yes");
    } else {
      window.open(urlPBC, "_system", "location=yes,enableviewportscale=yes");
    }
  };

  function getBrochureLink() {
    try {
      $http({
        url: URLAG + "/GetPlanBrochure",
        method: "POST",
        data: { strAgentCode: EncryptDecryptData.Encrypt(strASCode) }
      }).then(
        function(response) {
          $scope.urlPBC = response.data.d;
          if ($scope.urlPBC == "") {
            $scope.hideMe = true;
          } else {
            $scope.hideMe = false;
          }

          // if (ionic.Platform.platform()  == "ios") {
          //   window.open(
          //     urlPBC,
          //     "_system",
          //     "location=yes,enableviewportscale=yes"
          //   );
          // } else {
          //   window.open(
          //     urlPBC,
          //     "_system",
          //     "location=yes,enableviewportscale=yes"
          //   );
          // }

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.LogOutTouchPoint = function() {
    strIDCard = "";
    strClntnum = "";
    strChdrnum = "";
    strQuestion = "";
    strAnswer = "";
    strPassword = "";
    strUserid = "";
    strName_ins = "";
    strLastname_ins = "";
    strMobile_no = "";
    dtMobile_dt = "";
    strPrev_mobile = "";
    strEmail = "";
    dtEmail_dt = "";
    strPrev_email = "";
    strAgreement = "";
    dtUpddate = "";
    dtRegister_dt = "";
    strPhone = "";
    strPrev_phone = "";
    dtPhone_dt = "";
    strReset_password = "";
    guidReset_code = "";
    dtReset_date = "";
    strSend_life_asia = "";
    dtSend_dt = "";

    strUserImagePath = "";
    strUniqueCode = "";

    $ionicLoading.show({ template: "Logging out...." });
    //$localstorage.set('Loggin', '');

    // PEN TEST
    LogOut();

    $timeout(function() {
      $ionicLoading.hide();
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
      //$state.go('www.google.co.th');
      $location.path("/Login");
    }, 3000);
  };

  $scope.privacyPolicy = () => {
    var ref = null;

    if (ionic.Platform.platform() == "ios") {
      ref = cordova.InAppBrowser.open(
        urlPDP,
        "_blank",
        "usewkwebview=yes,hidden=yes,location=no,hideurlbar=yes,closebuttoncaption=ปิด"
      );
    } else {
      ref = cordova.InAppBrowser.open(
        urlPDP,
        "_blank",
        "hidden=yes,location=no,hideurlbar=yes,closebuttoncaption=ปิด,footer=yes"
      );
    }

    ref.show();
  };

  function LogOut() {
    // Call service LogOut for delete Token on server
    try {
      $http({
        url: URLAG + "/LogOut",
        method: "POST",
        data: {}
      }).then(
        function(response) {
          // Success
        },
        function(response) {
          // Fail
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }

    // Delete Token on local storage
    // TokenManager.DelBasic();
    // TokenManager.Del();

    // // Reset http default header
    // $http.defaults.headers.common.Authorization =
    //   "Bearer" +
    //   " " +
    //   TokenManager.Get() +
    //   ",Basic" +
    //   " " +
    //   TokenManager.GetBasic();
    JWTService.DelBasic();
    JWTService.Del();

    // Reset http default header
    $http.defaults.headers.common.Authorization =
      "Bearer" +
      " " +
      JWTService.Get() +
      ",Basic" +
      " " +
      JWTService.GetBasic();
  }
});
