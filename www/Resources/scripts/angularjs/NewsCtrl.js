CPApp.controller("NewsCtrl", function(
  $scope,
  $stateParams,
  $http,
  $timeout,
  $ionicLoading,
  TMLTHService
) {
  // console.log("NewsCtrl");

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  //Param To News Detail
  $scope.id = $stateParams.id;

  //debugger;
  var NewsID = "";
  if ($scope.id != undefined) {
    NewsID = $scope.id;
  }

  try {
    $http({
      url: URLNews + "/GetNewsHighlight",
      method: "POST",
      data: { strLng: lng, strNewsID: NewsID, strNewsClient: AppClient }
    }).then(
      function(response) {
        $scope.ListNewsHighlight = response.data.d;

        $timeout(function() {
          $ionicLoading.hide();
        }, 2000);
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Error");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  try {
    $http({
      url: URLNews + "/GetNews",
      method: "POST",
      data: { strLng: lng, strNewsID: NewsID, strNewsClient: AppClient }
    }).then(
      function(response) {
        $scope.ListNews = response.data.d;

        $timeout(function() {
          $ionicLoading.hide();
        }, 2000);
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Error");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  //Param To News Detail
  $scope.id = $stateParams.id;
});

/*CPApp.factory('dealerService', function($http) {
 return {
 getDealerList function() {
 var dealers = {
 list : []
 };
 // TODO add possible caching via $cacheFactory
 $http.get('files/js/dealer.json').success(function(data) {
 dealers.list = data;
 });
 return dealers;
 },
 
 // other functions
 };
 
 });
 
 
 CPApp.filter('startsWithLetter', function () {
 return function (items, letter) {
 var filtered = [];
 var letterMatch = new RegExp(letter, 'i');
 for (var i = 0; i < items.length; i++) {
 var item = items[i];
 if (letterMatch.test(item.name.substring(0, 1))) {
 filtered.push(item);
 }
 }
 return filtered;
 };
 });*/
