/*CPApp.controller('DownloadCtrl', function($scope, $timeout, $cordovaFileTransfer){*/
CPApp.controller("DownloadCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  $ionicLoading,
  $ionicModal,
  TMLTHService,
  EncryptDecryptData
) {
  // console.log("DownloadCtrl");

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  //alert('Download');

  $scope.URLSubPath = "TouchPoint/ForDownload/DownloadDocument/Static/";

  //Param To News Detail
  $scope.id = $stateParams.idx;

  //debugger;
  var DLID = "";
  if ($scope.id != undefined) {
    DLID = $scope.id;
  }

  if (DLID != "") {
    //alert(DLID);
    try {
      $http({
        url: URLDownload + "/GetStaticDocumentForDownloadByGroup",
        method: "POST",
        data: { strLng: lng, intGrp: DLID }
      }).then(
        function(response) {
          $scope.ListStaticDocument = response.data.d;
          $scope.DocTitle = $scope.ListStaticDocument[0].DocGrpNM;

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  } else {
    $timeout(function() {
      $ionicLoading.hide();
    }, 2000);
  }

  $scope.MailFrom_Modal = {};
  $scope.MailFrom = {
    text: "fill me in"
  };

  $scope.MailTo_Modal = {};
  $scope.MailTo = {
    text: "fill me in"
  };

  $scope.MailCC_Modal = {};
  $scope.MailCC = {
    text: "fill me in"
  };

  $scope.MailBCC_Modal = {};
  $scope.MailBCC = {
    text: "fill me in"
  };

  $scope.MailSubject_Modal = {};
  $scope.MailSubject = {
    text: "fill me in"
  };

  $scope.MailDetail_Modal = {};
  $scope.MailDetail = {
    text: "fill me in"
  };

  //$scope.MailTo_Modal.text = strEmail; //Case Agent: จะไม่มี Email ของ Cust ใน tpolicy;
  $scope.MailTo_Modal.text = "";

  $scope.clearModalText = function() {
    $scope.MailFrom_Modal.text = "";
    //console.log("clicked");
  };

  $scope.SendMailProcess = function(IsSendMail) {
    if (IsSendMail) {
      IsSendMail = false;
      $scope.MailFrom.text = $scope.MailFrom_Modal.text;
      $scope.MailTo.text = $scope.MailTo_Modal.text;
      $scope.MailCC.text = $scope.MailCC_Modal.text;
      $scope.MailBCC.text = $scope.MailBCC_Modal.text;

      $scope.MailSubject.text = $scope.MailSubject_Modal.text;
      $scope.MailDetail.text = $scope.MailDetail_Modal.text;

      if ($scope.MailTo.text == "") {
        alert("กรุณากรอกอีเมล์ผู้รับ");
        return;
      }

      if (
        ValidateMailTo($scope.MailTo.text) == false ||
        ValidateMailCC($scope.MailCC.text) == false ||
        ValidateMailBCC($scope.MailBCC.text) == false
      ) {
        alert("รูปแบบอีเมล์ไม่ถูกต้อง");
        return;
      }

      /*Send Mail*/
      try {
        if ($scope.MailCC.text == undefined) {
          $scope.MailCC.text = "";
        }
        if ($scope.MailBCC.text == undefined) {
          $scope.MailBCC.text = "";
        }
        if ($scope.MailSubject.text == undefined) {
          $scope.MailSubject.text = "";
        }
        if ($scope.MailDetail.text == undefined) {
          $scope.MailDetail.text = "";
        }

        $http({
          url: URLSendMail + "/SendFileStaticToMail",
          method: "POST",
          data: {
            strMailFrom: varMailFrom,
            strMailTo: $scope.MailTo.text,
            strMailBcc: $scope.MailBCC.text,
            strMailCC: $scope.MailCC.text,
            strSubject: $scope.MailSubject.text,
            strBody: $scope.MailDetail.text,
            isBodyHtml: "true",
            strURLFile: $scope.FPFN
          }
        }).then(
          function(response) {
            alert("Send Mail Complete");
          },
          function(response) {
            if (response.status == 401) {
              TMLTHService.unauthorizedService();
            } else {
              alert("Send Mail Fail");
              alert(response.data.Message);
            }
          }
        );
      } catch (err) {
        alert("Error : " + err.message);
      }
    }

    $scope.MailFrom_Modal.text = "";
    $scope.MailTo_Modal.text = "";
    $scope.MailCC_Modal.text = "";
    $scope.MailBCC_Modal.text = "";

    $scope.MailSubject_Modal.text = "";
    $scope.MailDetail_Modal.text = "";

    //$scope.MailTo_Modal.text = strEmail; //Case Agent: จะไม่มี Email ของ Cust ใน tpolicy;
    $scope.MailTo_Modal.text = "";
    $scope.closeModal();
  };

  /*Chk format Email*/

  function ValidateEmailFormat(strEmail) {
    var email = strEmail;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      return false;
    } else {
      return true;
    }
  }

  $scope.ValidateMailFromFormat = function(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      $scope.MailFromMsg = "";
    } else {
      $scope.MailFromMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailToFormat = function(strEmail) {
    $scope.MailToMsg = "";
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    if (bl == true) {
      $scope.MailToMsg = "";
    } else {
      $scope.MailToMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailCCFormat = function(strEmail) {
    $scope.MailCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    if (bl == true) {
      $scope.MailCCMsg = "";
    } else {
      $scope.MailCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailBCCFormat = function(strEmail) {
    $scope.MailBCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    if (bl == true) {
      $scope.MailBCCMsg = "";
    } else {
      $scope.MailBCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailToFormat = function(strEmail) {
    $scope.MailToMsg = "";
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailToMsg = "";
    } else {
      $scope.MailToMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailCCFormat = function(strEmail) {
    $scope.MailCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailCCMsg = "";
    } else {
      $scope.MailCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailBCCFormat = function(strEmail) {
    $scope.MailBCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailBCCMsg = "";
    } else {
      $scope.MailBCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  function ValidateMailFrom(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      //alert("T");
      return true;
    } else {
      //alert("F");
      return false;
    }
  }

  function ValidateMailTo(strEmail) {
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }

  function ValidateMailCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  function ValidateMailBCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  /*End Chk format Email*/

  //////////////////////////////////////

  function DownloadAndSendMail(strDocFullFileNM) {
    try {
      //alert(strDocFullFileNM);
      //alert('FTP Success');

      var strFPFN = strDocFullFileNM;
      $scope.FPFN = strFPFN;

      var str1 = $scope.FPFN;
      str1 = str1.substr($scope.FPFN.lastIndexOf("/") + 1);

      $scope.FN = "";

      $scope.MailAttached = str1;

      $scope.AttachedFile = { File: $scope.FPFN };

      $scope.MailFromMsg = "";
      $scope.MailToMsg = "";
      $scope.MailCCMsg = "";
      $scope.MailBCCMsg = "";

      $ionicModal
        .fromTemplateUrl("ModalSendMailDownload.tmpl.html", {
          scope: $scope,
          animation: "slide-in-up"
        })
        .then(function(modal) {
          $scope.modal = modal;

          $scope.openModal = function() {
            $scope.modal.show();
          };

          $scope.closeModal = function() {
            $scope.modal.hide();
          };

          $scope.$on("$destroy", function() {
            //$scope.modal.remove();
          });

          $scope.openModal();
        });
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.DownloadAndSendMailStatic = function(strDocFullFileNM, strMenu) {
    $scope.MailSubject_Modal.text = strMenu;

    // var strURL = URLServe + $scope.URLSubPath + strDocFullFileNM;
    var strURL = URLGetFile + $scope.URLSubPath + strDocFullFileNM;

    DownloadAndSendMail(strURL);
  };

  $scope.OpenModalSendMail = function(strDocFullFileNM, strDocType) {
    // $scope.MailSubject_Modal.text = strMenu;

    strDocFullFileNM = URLGetFile + $scope.URLSubPath + strDocFullFileNM;
    // alert(strURL);

    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      //debugger;
      $http({
        url: URLAG + "/GetAgentDetail",
        method: "POST",
        data: {
          strAsCode: EncryptDecryptData.Encrypt(strASCode)
        }
      }).then(
        function(response) {
          $ionicLoading.hide();
          // console.log(response.data.d[0]);

          $scope.AgentData = response.data.d[0];
          $scope.checkBTSendEmail = false;
          $scope.HasEmail = "Y";
          $scope.DocType = strDocType;
          $scope.DocfullNM = strDocFullFileNM;

          if ($scope.AgentData.Email == "" || !$scope.AgentData.Email) {
            $scope.HasEmail = "N";
          }

          $ionicModal
            .fromTemplateUrl("ModalSendMail_New.tmpl.html", {
              scope: $scope,
              animation: "slide-in-up"
            })
            .then(function(modal) {
              $scope.modal = modal;

              $scope.openModal = function() {
                $scope.modal.show();
              };

              $scope.closeModal = function() {
                $scope.modal.hide();
              };

              $scope.$on("$destroy", function() {
                //$scope.modal.remove();
              });

              $scope.openModal();
            });
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            // alert("FTP Fail");
            alert(response.data.Message);
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  };

  $scope.confirmSendEmail = (DocfullNM,DocType) => {
    $scope.checkBTSendEmail = true;
    console.log('strASCode : ' + strASCode)
    console.log('DocType : ' + DocType)
    console.log('DocfullNM : ' + DocfullNM)
    try {
      $http({
        url: URLSendMail + "/SendFileStaticToMail_New",
        method: "POST",
        data: {
          strAsCode: EncryptDecryptData.Encrypt(strASCode),
          strSubject: EncryptDecryptData.Encrypt(DocType),
          strDocPathFullNM: EncryptDecryptData.Encrypt(DocfullNM)
        }
      }).then(
        function(response) {
          alert("ส่งอีเมลแล้ว");
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert(response.data.Message);
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
    $scope.modal.hide();
  };

  $scope.GoToSetting = () => {
    $scope.modal.hide();

    // $ionicHistory.nextViewOptions({
    //   disableBack: true
    // });

    $state.go("app.ProfileSetting");
  };

  $scope.cancelSendEmail = () => {
    $scope.modal.hide();
  };

  $scope.SampleDownload = function() {
    $state.go("app.PDFViewer");
  };

  $scope.ViewFile = function downloadFile(FileNM) {
    // var strURL = URLServe + $scope.URLSubPath + FileNM;
    var strURL = URLGetFile + $scope.URLSubPath + FileNM;
    var ref = null;
    if (ionic.Platform.platform() == "ios") {
      ref = cordova.InAppBrowser.open(
        encodeURI(strURL),
        "_blank",
        "usewkwebview=yes,hidden=yes,location=no,hideurlbar=yes,closebuttoncaption=ปิด"
      );
    } else {
      ref = cordova.InAppBrowser.open(
        encodeURI(strURL),
        "_system",
        "hidden=yes,location=no"
      );
    }
    ref.show();
  };

  $scope.IndexValue = "";
  $scope.GoToDownload = function(ValIdx) {
    //alert(ValIdx);

    var val = ValIdx;

    ValIdx = "";
    $state.go("app.DownloadDetail", { idx: val }, { reload: true });
  };

  try {
    //alert('GetAllStaticDocumentForDownload');
    $http({
      url: URLDownload + "/GetAllStaticDocumentForDownload",
      method: "POST",
      data: { strLng: lng }
    }).then(
      function(response) {
        //alert('Success');
        $scope.LstStaticDocument = response.data.d;

        //There are 4 group
        $scope.DLDocuments = [];
        for (var i = 0; i < 5; i++) {
          $scope.DLDocuments[i] = {
            DocGrpNM: "x",
            DocNM: [],
            DocFN: [],
            DocNMFN: [],
            show: false
          };

          for (j = 0; j <= $scope.LstStaticDocument.length - 1; j++) {
            if ($scope.LstStaticDocument[j].DocGrpID == i + 1) {
              $scope.DLDocuments[i].DocGrpNM =
                $scope.LstStaticDocument[j].DocGrpNM;

              $scope.DLDocuments[i].DocNM.push(
                $scope.LstStaticDocument[j].DocNM
              );
              $scope.DLDocuments[i].DocFN.push(
                $scope.LstStaticDocument[j].DocFN
              );

              $scope.DLDocuments[i].DocNMFN.push(
                $scope.LstStaticDocument[j].DocNM +
                  "|||" +
                  $scope.LstStaticDocument[j].DocFN
              );
            }
          }
        }
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Fail");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  $scope.toggleGroupDoc = function(groupDoc) {
    groupDoc.show = !groupDoc.show;
  };
  $scope.isGroupDocShown = function(groupDoc) {
    return groupDoc.show;
  };

  $scope.GetDocNMFromNMFN = function(val) {
    //alert('GetDocNMFromNMFN');
    var ret = "";
    if (val != undefined && val != "") {
      var res = val.split("|||");
      ret = res[0];
    }

    return ret;
  };

  $scope.GetDocFNFromNMFN = function(val) {
    //alert('GetDocFNFromNMFN');
    var ret = "";
    if (val != undefined && val != "") {
      var res = val.split("|||");
      ret = res[1];
    }

    return ret;
  };

  $scope.groups = [];
  for (var i = 0; i < 10; i++) {
    $scope.groups[i] = {
      name: i,
      items: [],
      show: false
    };
    for (var j = 0; j < 3; j++) {
      $scope.groups[i].items.push(i + "-" + j);
    }
  }
});
