CPApp.directive("clickForOptions", [
  "$ionicGesture",
  function($ionicGesture) {
    return {
      restrict: "A",
      link: function(scope, element, attrs) {
        $ionicGesture.on(
          "tap",
          function(e) {
            // Grab the content
            var content = element[0].querySelector(".item-content");

            // Grab the buttons and their width
            var buttons = element[0].querySelector(".item-options");

            if (!buttons) {
              console.log("There are no option buttons");
              return;
            }
            var buttonsWidth = buttons.offsetWidth;

            ionic.requestAnimationFrame(function() {
              content.style[ionic.CSS.TRANSITION] = "all ease-out .25s";

              if (!buttons.classList.contains("invisible")) {
                console.log("close");
                content.style[ionic.CSS.TRANSFORM] = "";
                setTimeout(function() {
                  buttons.classList.add("invisible");
                }, 250);
              } else {
                buttons.classList.remove("invisible");
                content.style[ionic.CSS.TRANSFORM] =
                  "translate3d(-" + buttonsWidth + "px, 0, 0)";
              }
            });
          },
          element
        );
      }
    };
  }
]);

CPApp.controller("PaymentLogCtrl", function(
  $scope,
  $http,
  $timeout,
  $ionicLoading,
  $ionicModal,
  TMLTHService
) {
  //console.log("PaymentLogCtrl");

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $scope.HasData = "Y";

  try {
    $http({
      url: URLInsure + "/GetInsuranceByClientNumber",
      method: "POST",
      data: { strClientNumber: strClntnum }
    }).then(
      function(response) {
        //alert('Success');

        $scope.ListInsuranceNo = response.data.d;

        if (response.data.d != "") {
          $scope.HasData = "Y";
          $scope.DefaultListInsuranceNo = response.data.d[0].Chdrnum;
          fnGetPaymentLogByChdrnum($scope.DefaultListInsuranceNo);

          $scope.ddlSelectChdrnum = $scope.DefaultListInsuranceNo;
        } else {
          $scope.HasData = "N";
          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        }
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Error");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  function fnGetPaymentLogByChdrnum(val) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    fnGetAllPaymentDocument(val);

    // /*Payment Log*/
    // try {
    //       $http({
    //           url: URLInsure + "/GetPaymentLog",
    //           method: "POST",
    //           data: { 'strPolicyNo': val}
    //       })
    //       .then(function(response) {

    //       //alert('Card Success');

    //       //$scope.ListHospital = response.data.d;

    //       $scope.ListPaymentLog = response.data.d;

    // 			fnGetAllPaymentDocument(val);

    //       // success
    //       //debugger;
    //       },
    //       function(response) { // optional
    //         // failed
    //         //alert('Card Fail');
    //         //debugger;
    //       });

    //   } catch (err) {
    //       alert('Error : ' + err.message);
    //   }
  }

  $scope.ddlSelectChdrnum = ""; //Insure ID
  $scope.GetPaymentLogByChdrnum = function(val) {
    fnGetPaymentLogByChdrnum(val);
  };

  function GetPOS001(ChdrnumVal) {
    var DocTPVal = "POS001";
    try {
      $http({
        url: URLInsure + "/GetPaymentDocument",
        method: "POST",
        data: { strChdrnum: ChdrnumVal.trim(), strDoctype: DocTPVal.trim() }
      }).then(
        function(response) {
          $scope.DocPOS001 = response.data.d;

          GetPOS014(ChdrnumVal);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }
  function GetPOS014(ChdrnumVal) {
    var DocTPVal = "POS014";
    try {
      $http({
        url: URLInsure + "/GetPaymentDocument",
        method: "POST",
        data: { strChdrnum: ChdrnumVal.trim(), strDoctype: DocTPVal.trim() }
      }).then(
        function(response) {
          $scope.DocPOS014 = response.data.d;

          GetPOS037(ChdrnumVal);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }
  function GetPOS037(ChdrnumVal) {
    var DocTPVal = "POS037";
    try {
      $http({
        url: URLInsure + "/GetBillPaymentAndDoc",
        method: "POST",
        data: { strChdrnum: ChdrnumVal.trim() }
      }).then(
        function(response) {
          $scope.DocPOS037 = response.data.d;

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function fnGetAllPaymentDocument(ChdrnumVal) {
    GetPOS001(ChdrnumVal);
  }

  $scope.numberWithCommas = function(x) {
    //alert(x);
    var parts = x.toString().split(".");
    //alert(parts);
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  };

  $scope.ConvertJSONDateToString = function(DateVal) {
    var dateString = DateVal.substr(6); //"\/Date(1334514600000)\/".substr(6);
    //alert(dateString);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var date = month + "/" + day + "/" + (parseInt(year) + 543).toString();
    //alert(date);
    return getFormattedDateTH(date);
  };

  $scope.ConvertYYYYMMDDDateToString = function(DateVal) {
    //alert(DateVal);
    var sDate = DateVal.toString();
    var year = sDate.substr(0, 4);
    var month = sDate.substr(4, 2);
    var day = sDate.substr(6, 2);

    var date = month + "/" + day + "/" + (parseInt(year) + 543).toString();
    //alert(date);
    return getFormattedDateTH(date);
  };

  function getFormattedDateEN(input) {
    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = input.replace(pattern, function(match, p1, p2, p3) {
      var months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ];
      return (p2 < 10 ? "" + p2 : p2) + " " + months[p1 - 1] + " " + p3;
    });

    return result;
  }

  function getFormattedDateTH(input) {
    var pattern = /(.*?)\/(.*?)\/(.*?)$/;
    var result = input.replace(pattern, function(match, p1, p2, p3) {
      var months = [
        "ม.ค.",
        "ก.พ.",
        "มี.ค.",
        "เม.ย.",
        "พ.ค.",
        "มิ.ย.",
        "ก.ค.",
        "ส.ค.",
        "ก.ย.",
        "ต.ค.",
        "พ.ย.",
        "ธ.ค."
      ];
      return (p2 < 10 ? "" + p2 : p2) + " " + months[p1 - 1] + " " + p3;
    });

    return result;
  }

  function ViewFile(URLFileVal) {
    //window.open(URLFileVal,'_blank');
    window.open(URLFileVal, "_blank", "location=yes,enableviewportscale=yes");
  }

  function FTPDoc(ChdrnumVal, CertDocType, DocfullNM) {
    try {
      $http({
        url: URLFtp + "/FTPPaymentDocument",
        method: "POST",
        data: {
          strChdrnum: ChdrnumVal,
          strDoctype: CertDocType,
          strDocPathFullNM: DocfullNM
        }
      }).then(
        function(response) {
          var strFPFN = response.data.d;
          $scope.FPFN = strFPFN;

          ViewFile(strFPFN);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("FTP Fail");
            alert(response.data.Message);
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.DownloadNoti = function(strDocFullFileNM) {
    var NotiDocType = "POS001";
    FTPDoc($scope.ddlSelectChdrnum, NotiDocType, strDocFullFileNM);
  };

  $scope.DownloadCert = function(strDocFullFileNM) {
    var CertDocType = "POS014";
    FTPDoc($scope.ddlSelectChdrnum, CertDocType, strDocFullFileNM);
  };

  $scope.DownloadBill = function(strDocFullFileNM) {
    var BillDocType = "POS037";
    FTPDoc($scope.ddlSelectChdrnum, BillDocType, strDocFullFileNM);
  };

  $scope.MailFrom_Modal = {};
  $scope.MailFrom = {
    text: "fill me in"
  };

  $scope.MailTo_Modal = {};
  $scope.MailTo = {
    text: "fill me in"
  };

  $scope.MailCC_Modal = {};
  $scope.MailCC = {
    text: "fill me in"
  };

  $scope.MailBCC_Modal = {};
  $scope.MailBCC = {
    text: "fill me in"
  };

  $scope.MailSubject_Modal = {};
  $scope.MailSubject = {
    text: "fill me in"
  };

  $scope.MailDetail_Modal = {};
  $scope.MailDetail = {
    text: "fill me in"
  };

  $scope.MailTo_Modal.text = strEmail;

  $scope.clearModalText = function() {
    $scope.MailFrom_Modal.text = "";
    //console.log("clicked");
  };

  $scope.SendMailProcess = function(IsSendMail) {
    if (IsSendMail) {
      IsSendMail = false;
      $scope.MailFrom.text = $scope.MailFrom_Modal.text;
      $scope.MailTo.text = $scope.MailTo_Modal.text;
      $scope.MailCC.text = $scope.MailCC_Modal.text;
      $scope.MailBCC.text = $scope.MailBCC_Modal.text;

      $scope.MailSubject.text = $scope.MailSubject_Modal.text;
      $scope.MailDetail.text = $scope.MailDetail_Modal.text;

      // if($scope.MailFrom.text == "" || $scope.MailTo.text == "")
      //                       {
      //                           alert("กรุณากรอกอีเมล์ผู้รับ/ผู้ส่ง");
      //                           return;
      //                       }

      if ($scope.MailTo.text == "") {
        alert("กรุณากรอกอีเมล์ผู้รับ");
        return;
      }

      if (
        ValidateMailTo($scope.MailTo.text) == false ||
        ValidateMailCC($scope.MailCC.text) == false ||
        ValidateMailBCC($scope.MailBCC.text) == false
      ) {
        alert("รูปแบบอีเมล์ไม่ถูกต้อง");
        return;
      }

      /*Send Mail*/
      try {
        if ($scope.MailCC.text == undefined) {
          $scope.MailCC.text = "";
        }
        if ($scope.MailBCC.text == undefined) {
          $scope.MailBCC.text = "";
        }
        if ($scope.MailSubject.text == undefined) {
          $scope.MailSubject.text = "";
        }
        if ($scope.MailDetail.text == undefined) {
          $scope.MailDetail.text = "";
        }

        $http({
          url: URLSendMail + "/SendFileToMail",
          method: "POST",
          data: {
            strMailFrom: varMailFrom,
            strMailTo: $scope.MailTo.text,
            strMailBcc: $scope.MailBCC.text,
            strMailCC: $scope.MailCC.text,
            strSubject: $scope.MailSubject.text,
            strBody: $scope.MailDetail.text,
            isBodyHtml: "true",
            strURLFile: $scope.FPFN
          }
          /*data: { 'strMailFrom': $scope.MailFrom.text,'strMailTo':$scope.MailTo.text,'strMailBcc':$scope.MailBCC.text,'strMailCC':$scope.MailCC.text,'strSubject':$scope.MailSubject.text,'strBody':$scope.MailDetail.text,'isBodyHtml':"true",'strURLFile':$scope.FPFN}*/
        }).then(
          function(response) {
            alert("Send Mail Complete");
          },
          function(response) {
            if (response.status == 401) {
              TMLTHService.unauthorizedService();
            } else {
              alert("Send Mail Fail");
              alert(response.data.Message);
            }
          }
        );
      } catch (err) {
        alert("Error : " + err.message);
      }
    }
    $scope.MailFrom_Modal.text = "";
    $scope.MailTo_Modal.text = "";
    $scope.MailCC_Modal.text = "";
    $scope.MailBCC_Modal.text = "";

    $scope.MailSubject_Modal.text = "";
    $scope.MailDetail_Modal.text = "";

    $scope.MailTo_Modal.text = strEmail;

    $scope.closeModal();
  };

  // $scope.ModalSendMail = function() {

  // $ionicModal.fromTemplateUrl("ModalSendMail.tmpl.html", {
  // scope: $scope,
  // animation: 'slide-in-up'
  // }).then(function(modal) {

  // $scope.modal = modal;

  // $scope.openModal = function() {
  // $scope.modal.show();
  // };

  // $scope.closeModal = function() {
  // $scope.modal.hide();
  // };

  // $scope.$on('$destroy', function() {
  // //$scope.modal.remove();
  // });

  // $scope.openModal();
  // });

  // };

  //////////////////////////////////////

  function DownloadAndSendMail(DocType, strDocFullFileNM) {
    try {
      $http({
        url: URLFtp + "/FTPPaymentDocument",
        method: "POST",
        data: {
          strChdrnum: $scope.ddlSelectChdrnum,
          strDoctype: DocType,
          strDocPathFullNM: strDocFullFileNM
        }
      }).then(
        function(response) {
          var strFPFN = response.data.d;
          $scope.FPFN = strFPFN;

          var str1 = $scope.FPFN;
          str1 = str1.substr($scope.FPFN.lastIndexOf("/") + 1);

          $scope.FN = "";

          $scope.MailAttached = str1;

          $scope.AttachedFile = { File: $scope.FPFN };

          $scope.MailFromMsg = "";
          $scope.MailToMsg = "";
          $scope.MailCCMsg = "";
          $scope.MailBCCMsg = "";

          $ionicModal
            .fromTemplateUrl("ModalSendMail.tmpl.html", {
              scope: $scope,
              animation: "slide-in-up"
            })
            .then(function(modal) {
              $scope.modal = modal;

              $scope.openModal = function() {
                $scope.modal.show();
              };

              $scope.closeModal = function() {
                $scope.modal.hide();
              };

              $scope.$on("$destroy", function() {
                //$scope.modal.remove();
              });

              $scope.openModal();
            });
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("FTP Fail");
            alert(response.data.Message);
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.DownloadAndSendMailNoti = function(strDocFullFileNM) {
    var NotiDocType = "POS001";

    DownloadAndSendMail(NotiDocType, strDocFullFileNM);
  };

  $scope.DownloadAndSendMailCert = function(strDocFullFileNM) {
    var NotiDocType = "POS014";

    DownloadAndSendMail(NotiDocType, strDocFullFileNM);
  };

  $scope.DownloadAndSendMailBill = function(strDocFullFileNM) {
    var NotiDocType = "POS037";

    DownloadAndSendMail(NotiDocType, strDocFullFileNM);
  };

  /*Chk format Email*/
  $scope.ValidateMailFromFormat = function(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      $scope.MailFromMsg = "";
    } else {
      $scope.MailFromMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailToFormat = function(strEmail) {
    $scope.MailToMsg = "";
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailToMsg = "";
    } else {
      $scope.MailToMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailCCFormat = function(strEmail) {
    $scope.MailCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailCCMsg = "";
    } else {
      $scope.MailCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailBCCFormat = function(strEmail) {
    $scope.MailBCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailBCCMsg = "";
    } else {
      $scope.MailBCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  function ValidateEmailFormat(strEmail) {
    var email = strEmail;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      //alert('Please provide a valid email address');
      //email.focus;
      //$scope.InvalidEmailFormatMsg='Email ไม่ถูกรูปแบบ';
      //$scope.CanSubmit = false;
      return false;
    } else {
      //$scope.InvalidEmailFormatMsg='';
      //$scope.CanSubmit = true;
      return true;
    }
  }

  function ValidateMailFrom(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      //alert("T");
      return true;
    } else {
      //alert("F");
      return false;
    }
  }
  function ValidateMailTo(strEmail) {
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  function ValidateMailCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  function ValidateMailBCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  /*End Chk format Email*/
});
