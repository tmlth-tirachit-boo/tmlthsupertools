angular.module('ionicApp', ['ionic'])

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position("top");



  //$ionicConfigProvider.views.transition('android');

  $stateProvider
  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.welcome', {
    url: "/welcome",
    views: {
      'menuContent': {
        templateUrl: "templates/welcome.html",
        controller: 'WelcomeCtrl'
      }
    }
  })

  .state('app.login', {
    url: "/login",
    views: {
      'menuContent': {
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      }
    }
  })

  .state('app.tabs1', {
    url: "/tabs1",
    abstract: true,
    views: {
      'menuContent': {
        templateUrl: "templates/tabs1.html"
      }
    }
  })

  .state('app.tabs1.home1', {
    url: "/home1",
    views: {
      'home1': {
        templateUrl: "templates/tabs1-home1.html"
      }
    }
  })

  .state('app.tabs1.about1', {
    url: "/about1",
    views: {
      'about1': {
        templateUrl: "templates/tabs1-about1.html"
      }
    }
  })

  .state('app.tabs1.settings1', {
    url: "/settings1",
    views: {
      'settings1': {
        templateUrl: "templates/tabs1-settings1.html"
      }
    }
  })

  .state('app.tabs2', {
    url: "/tabs2",
    abstract:true,
    views: {
      'menuContent': {
        templateUrl: "templates/tabs2.html"
      }
    }
  })

  .state('app.tabs2.home2', {
    url: "/home2",
    views: {
      'home2': {
        templateUrl: "templates/tabs2-home2.html"
      }
    }
  })

  .state('app.tabs2.about2', {
    url: "/about2",
    views: {
      'about2': {
        templateUrl: "templates/tabs2-about2.html"
      }
    }
  })

  .state('app.tabs2.settings2', {
    url: "/settings2",
    views: {
      'settings2': {
        templateUrl: "templates/tabs2-settings2.html"
      }
    }
  });


  $urlRouterProvider.otherwise("/app/welcome");
  //$ionicConfigProvider.views.maxCache(0);
})

.controller('AppCtrl', function($scope) {
  console.log('AppCtrl');
})

.controller('WelcomeCtrl', function($scope) {
  console.log('WelcomeCtrl');
})

.controller('LoginCtrl', function($scope) {
  console.log('LoginCtrl');
});
