CPApp.controller("HospitalCtrl", function(
  $scope,
  $http,
  $timeout,
  $ionicLoading,
  TMLTHService
) {
  // console.log("HospitalCtrl");

  //debugger;
  try {
    $http({
      url: URLLookup + "/GetHospitalProvince",
      method: "POST",
      data: { strLng: lng }
    }).then(
      function(response) {
        $scope.ListProvinceHospital = response.data.d;
        if (response.data.d != "") {
          if (strHospitalProvSelected == "") {
            strHospitalProvSelected = "กรุงเทพฯ";
          } else {
            strHospitalProvSelected = strHospitalProvSelected;
          }
          $scope.DefaultHospitalProvince = strHospitalProvSelected;
          fnGetHospitalByProvinceName($scope.DefaultHospitalProvince);
        }
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Error");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  $scope.ddlSelectProvinceValue = "";
  $scope.GetHospitalByProvince = function(NewValue) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      $http({
        url: URLLookup + "/GetHospital",
        method: "POST",
        data: { strLng: lng, strProvince: NewValue }
      }).then(
        function(response) {
          $scope.ListHospital = response.data.d;
          strHospitalProvSelected = NewValue;
          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  };

  $scope.ConvertToPhoneFormat = function(val) {
    var retVal = "";
    var AllVal = val.split(",");
    for (i = 0; i <= AllVal.length - 1; i++) {
      AllVal[i] = AllVal[i].trim();
      //AllVal[i] = AllVal[i].replace("-", "");
      if (AllVal[i].length == 9) {
        var preCode = "";
        preCode = AllVal[i].substring(0, 2);
        if (preCode == "02") {
          AllVal[i] = AllVal[i].replace(/(\d{2})(\d{3})(\d{4})/, "$1-$2-$3");
        } else {
          AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{3})/, "$1-$2-$3");
        }
        //AllVal[i] = AllVal[i];
      } else if (AllVal[i].length == 10) {
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      } else {
        //AllVal[i] = AllVal[i];
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d)/, "$1-$2-$3");
      }
      retVal = retVal + AllVal[i] + ",";
    }
    if (retVal != "") {
      retVal = retVal.substring(0, retVal.length - 1);
    }
    return retVal;
  };

  function fnGetHospitalByProvinceName(NewValue) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      $http({
        url: URLLookup + "/GetHospital",
        method: "POST",
        data: { strLng: lng, strProvince: NewValue }
      }).then(
        function(response) {
          $scope.ListHospital = response.data.d;
          $timeout(function() {
            $ionicLoading.hide();
          }, 4000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }
});
