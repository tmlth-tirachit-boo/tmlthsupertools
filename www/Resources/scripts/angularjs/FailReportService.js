CPApp.factory("FailReportService", function($http,EncryptDecryptData) {
  var UnitMemberList = [];
  var AgentMemberList = [];
  var CustomerList = [];
  var FailDataDetail = [];

  return {
    getUnit: function(strASCode) {
      return $http
        .post(URLAG + "/GetUnitListPaymentFailReport", { strAscode: strASCode })
        .then(function(response) {
          UnitMemberList = response.data.d;
          return UnitMemberList;
        });
    },
    getAgent: function(strUnitco) {
      return $http
        .post(URLAG + "/GetAgentListPaymentFailReport_New", {
          strUnitco: EncryptDecryptData.Encrypt(strUnitco),
          strPaymentType: EncryptDecryptData.Encrypt(strPaymentType)
        })
        .then(function(response) {

          AgentMemberList = response.data.d;
          return AgentMemberList;
        });
    },
    getCustomer: function(ASCode) {
      return $http
        .post(URLAG + "/GetCustomerListPaymentFailReport_New", {
          strAsCode: EncryptDecryptData.Encrypt(ASCode),
          strPaymentType: EncryptDecryptData.Encrypt(strPaymentType)
        })
        .then(function(response) {
          //console.log(response);
          CustomerList = response.data.d;
          return CustomerList;
        });
    },
    getFailReportDetail: function(Chdrnum, Ascode) {
      var pPaymentType = strPaymentType;

      return $http
        .post(URLAG + "/GetCustomerListPaymentFailReportDetail_New", {
          strChdrnum: EncryptDecryptData.Encrypt(Chdrnum),
          strAsCode: EncryptDecryptData.Encrypt(Ascode),
          strPaymentType: EncryptDecryptData.Encrypt(pPaymentType)
        })
        .then(function(response) {
          //console.log(response);
          FailDataDetail = response.data.d;
          return FailDataDetail;
        });
    }
  };
});
