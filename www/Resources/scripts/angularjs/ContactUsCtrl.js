CPApp.controller("ContactUsCtrl", function(
  $scope,
  $http,
  $timeout,
  $ionicLoading,
  $ionicModal,
  TMLTHService
) {
  // console.log("ContactUsCtrl");

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  readDeviceOrientation();

  window.onorientationchange = readDeviceOrientation;

  function readDeviceOrientation() {
    if (Math.abs(window.orientation) === 90) {
      // Landscape

      if (window.innerWidth <= 568) {
        //Mobile Landscape
        $scope.CTC1Caption =
          "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";
        $scope.CTC1Val =
          "<a href='tel: 02-650-1400' class='CTCText'>02-650-1400</a> กด 1";

        $scope.CTC2Caption = "<div class='CTCCaption'>ส่งอีเมล์ถึงเรา</div>";
        $scope.CTC2Val =
          "<div class='CTCText'>customervoice@tokiomarinelife.co.th</div>";

        $scope.CTC3Caption =
          "<div class='CTCCaption'>สอบถามอื่นๆ และความคิดเห็น</div>";
        $scope.CTC3Val =
          "<a href='tel: 02-670-4140' class='CTCText'>02-670-4140</a>";

        $scope.CTC4Caption =
          "<div class='CTCCaption'>มาเยี่ยมชม หรือ เขียนจดหมายถึงเราที่</div>";
        $scope.CTC4Val =
          "<div class='CTCText'>บมจ. โตเกียวมารีนประกันชีวิต (ประเทศไทย)</div><div class='CTCText'>เลขที่ 1 อาคารเอ็มไพร์ทาวเวอร์ ชั้น 26 ถนนสาทรใต้ แขวงยานนาวา เขตสาทร กรุงเทพฯ 10120</div>";
      } else if (window.innerWidth >= 1024) {
        //mini Pad up Landscape
        objCTC1Caption = "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";

        $scope.CTC1Caption =
          "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";
        $scope.CTC1Val =
          "<a href='tel: 02-650-1400' class='CTCText'>02-650-1400</a> กด 1";

        $scope.CTC2Caption = "<div class='CTCCaption'>ส่งอีเมล์ถึงเรา</div>";
        $scope.CTC2Val =
          "<div class='CTCText'>customervoice@tokiomarinelife.co.th</div>";

        $scope.CTC3Caption =
          "<div class='CTCCaption'>สอบถามอื่นๆ และความคิดเห็น</div>";
        $scope.CTC3Val =
          "<a href='tel: 02-670-4140' class='CTCText'>02-670-4140</a>";

        $scope.CTC4Caption =
          "<div class='CTCCaption'>มาเยี่ยมชม หรือ เขียนจดหมายถึงเราที่</div>";
        $scope.CTC4Val =
          "<div class='CTCText'>บมจ. โตเกียวมารีนประกันชีวิต (ประเทศไทย)</div><div class='CTCText'>เลขที่ 1 อาคารเอ็มไพร์ทาวเวอร์ ชั้น 26</div><div class='CTCText'> ถนนสาทรใต้ แขวงยานนาวา เขตสาทร กรุงเทพฯ 10120</div>";
      } else {
        objCTC1Caption = "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";

        $scope.CTC1Caption =
          "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";
        $scope.CTC1Val =
          "<a href='tel: 02-650-1400' class='CTCText'>02-650-1400</a> กด 1";

        $scope.CTC2Caption = "<div class='CTCCaption'>ส่งอีเมล์ถึงเรา</div>";
        $scope.CTC2Val =
          "<div class='CTCText'>customervoice@tokiomarinelife.co.th</div>";

        $scope.CTC3Caption =
          "<div class='CTCCaption'>สอบถามอื่นๆ และความคิดเห็น</div>";
        $scope.CTC3Val =
          "<a href='tel: 02-670-4140' class='CTCText'>02-670-4140</a>";

        $scope.CTC4Caption =
          "<div class='CTCCaption'>มาเยี่ยมชม หรือ เขียนจดหมายถึงเราที่</div>";
        $scope.CTC4Val =
          "<div class='CTCText'>บมจ. โตเกียวมารีนประกันชีวิต (ประเทศไทย)</div><div class='CTCText'>เลขที่ 1 อาคารเอ็มไพร์ทาวเวอร์ ชั้น 26</div><div class='CTCText'> ถนนสาทรใต้ แขวงยานนาวา เขตสาทร กรุงเทพฯ 10120</div>";
      }
    } else {
      // Portrait

      if (window.innerWidth <= 480) {
        //Mobile Portrait
        $scope.CTC1Caption =
          "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";
        $scope.CTC1Val =
          "<a href='tel: 02-650-1400' class='CTCText'>02-650-1400</a> กด 1";

        $scope.CTC2Caption = "<div class='CTCCaption'>ส่งอีเมล์ถึงเรา</div>";
        $scope.CTC2Val =
          "<div class='CTCText'>customervoice@</div><div class='CTCText'>tokiomarinelife.co.th</div>";

        $scope.CTC3Caption =
          "<div class='CTCCaption'>สอบถามอื่นๆ</div><div class='CTCCaption'>และความคิดเห็น</div>";
        $scope.CTC3Val =
          "<a href='tel: 02-670-4140' class='CTCText'>02-670-4140</a>";

        $scope.CTC4Caption =
          "<div class='CTCCaption'>มาเยี่ยมชม หรือ </div><div class='CTCCaption'>เขียนจดหมายถึงเราที่</div>";
        $scope.CTC4Val =
          "<div class='CTCText'>บมจ. โตเกียวมารีนประกันชีวิต (ประเทศไทย)</div><div class='CTCText'>เลขที่ 1 อาคารเอ็มไพร์ทาวเวอร์ ชั้น 26</div><div class='CTCText'> ถนนสาทรใต้ แขวงยานนาวา เขตสาทร กรุงเทพฯ 10120</div>";
      } else if (window.innerWidth >= 768) {
        //mini Pad up Portrait
        $scope.CTC1Caption =
          "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";
        $scope.CTC1Val =
          "<a href='tel: 02-650-1400' class='CTCText'>02-650-1400</a> กด 1";

        $scope.CTC2Caption = "<div class='CTCCaption'>ส่งอีเมล์ถึงเรา</div>";
        $scope.CTC2Val =
          "<div class='CTCText'>customervoice@tokiomarinelife.co.th</div>";

        $scope.CTC3Caption =
          "<div class='CTCCaption'>สอบถามอื่นๆ และความคิดเห็น</div>";
        $scope.CTC3Val =
          "<a href='tel: 02-670-4140' class='CTCText'>02-670-4140</a>";

        $scope.CTC4Caption =
          "<div class='CTCCaption'>มาเยี่ยมชม หรือ </div><div class='CTCCaption'>เขียนจดหมายถึงเราที่</div>";
        $scope.CTC4Val =
          "<div class='CTCText'>บมจ. โตเกียวมารีนประกันชีวิต (ประเทศไทย)</div><div class='CTCText'>เลขที่ 1 อาคารเอ็มไพร์ทาวเวอร์ ชั้น 26</div><div class='CTCText'> ถนนสาทรใต้ แขวงยานนาวา เขตสาทร กรุงเทพฯ 10120</div>";
      } else {
        $scope.CTC1Caption =
          "<div class='CTCCaption'>บริการผู้ถือกรมธรรม์</div>";
        $scope.CTC1Val =
          "<a href='tel: 02-650-1400' class='CTCText'>02-650-1400</a> กด 1";

        $scope.CTC2Caption = "<div class='CTCCaption'>ส่งอีเมล์ถึงเรา</div>";
        $scope.CTC2Val =
          "<div class='CTCText'>customervoice@tokiomarinelife.co.th</div>";

        $scope.CTC3Caption =
          "<div class='CTCCaption'>สอบถามอื่นๆ และความคิดเห็น</div>";
        $scope.CTC3Val =
          "<a href='tel: 02-670-4140' class='CTCText'>02-670-4140</a>";

        $scope.CTC4Caption =
          "<div class='CTCCaption'>มาเยี่ยมชม หรือ </div><div class='CTCCaption'>เขียนจดหมายถึงเราที่</div>";
        $scope.CTC4Val =
          "<div class='CTCText'>บมจ. โตเกียวมารีนประกันชีวิต (ประเทศไทย)</div><div class='CTCText'>เลขที่ 1 อาคารเอ็มไพร์ทาวเวอร์ ชั้น 26</div><div class='CTCText'> ถนนสาทรใต้ แขวงยานนาวา เขตสาทร กรุงเทพฯ 10120</div>";
      }
    }

    setTimeout(function() {
      $scope.$apply(function() {
        //$scope.message = "Timeout called!";
        //$scope.XXX=objCTC1Caption;
      });
    }, 10);
  }

  try {
    $http({
      url: URLContactUs + "/GetContactUs",
      method: "POST",
      data: { strLng: lng }
    }).then(
      function(response) {
        $scope.ListContactUs = response.data.d;

        $timeout(function() {
          $ionicLoading.hide();
        }, 2000);
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Fail");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  $scope.GoToFormOnline = function(URLVal) {
    
    if (ionic.Platform.platform() == "ios") {
      window.open(URLVal, '_blank', 'location=yes,enableviewportscale=yes');
    } else {
      window.open(URLVal, "_system", "location=yes,enableviewportscale=yes");
    }
  };

  $scope.ConvertToPhoneFormat = function(val) {
    var retVal = "";

    var AllVal = val.split(",");

    for (i = 0; i <= AllVal.length - 1; i++) {
      AllVal[i] = AllVal[i].trim();

      //AllVal[i] = AllVal[i].replace("-", "");

      if (AllVal[i].length == 9) {
        var preCode = "";
        preCode = AllVal[i].substring(0, 2);

        if (preCode == "02") {
          AllVal[i] = AllVal[i].replace(/(\d{2})(\d{3})(\d{4})/, "$1-$2-$3");
        } else {
          AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{3})/, "$1-$2-$3");
        }

        //AllVal[i] = AllVal[i];
      } else if (AllVal[i].length == 10) {
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      } else {
        //AllVal[i] = AllVal[i];
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d)/, "$1-$2-$3");
      }

      retVal = retVal + AllVal[i] + ",";
    }

    if (retVal != "") {
      retVal = retVal.substring(0, retVal.length - 1);
    }

    return retVal;
  };

  $scope.ConvertToPhoneWithOutExt = function(val) {
    var retVal = "";

    if (val != "") {
      retVal = val.substring(0, val.indexOf(" "));
    }

    return retVal;
  };

  $scope.MailFrom_Modal = {};
  $scope.MailFrom = {
    text: "fill me in"
  };

  $scope.MailTo_Modal = {};
  $scope.MailTo = {
    text: "fill me in"
  };

  $scope.MailCC_Modal = {};
  $scope.MailCC = {
    text: "fill me in"
  };

  $scope.MailBCC_Modal = {};
  $scope.MailBCC = {
    text: "fill me in"
  };

  $scope.MailSubject_Modal = {};
  $scope.MailSubject = {
    text: "fill me in"
  };

  $scope.MailDetail_Modal = {};
  $scope.MailDetail = {
    text: "fill me in"
  };

  $scope.MailTo_Modal.text = varMailFrom; //strEmail; //"aass";

  $scope.clearModalText = function() {
    $scope.MailFrom_Modal.text = "";
    //console.log("clicked");
  };

  $scope.SendMailProcess = function(IsSendMail) {
    if (IsSendMail) {
      IsSendMail = false;
      $scope.MailFrom.text = $scope.MailFrom_Modal.text;
      $scope.MailTo.text = $scope.MailTo_Modal.text;
      $scope.MailCC.text = $scope.MailCC_Modal.text;
      $scope.MailBCC.text = $scope.MailBCC_Modal.text;

      $scope.MailSubject.text = $scope.MailSubject_Modal.text;
      $scope.MailDetail.text = $scope.MailDetail_Modal.text;

      if ($scope.MailTo.text == "") {
        alert("กรุณากรอกอีเมล์ผู้รับ");
        return;
      }

      if (
        ValidateMailTo($scope.MailTo.text) == false ||
        ValidateMailCC($scope.MailCC.text) == false ||
        ValidateMailBCC($scope.MailBCC.text) == false
      ) {
        alert("รูปแบบอีเมล์ไม่ถูกต้อง");
        return;
      }

      /*Send Mail*/
      try {
        if ($scope.MailCC.text == undefined) {
          $scope.MailCC.text = "";
        }
        if ($scope.MailBCC.text == undefined) {
          $scope.MailBCC.text = "";
        }
        if ($scope.MailSubject.text == undefined) {
          $scope.MailSubject.text = "";
        }
        if ($scope.MailDetail.text == undefined) {
          $scope.MailDetail.text = "";
        }

        //alert(URLSendMail + "/SendFileStaticToMail");
        //alert(varMailFrom);
        $http({
          url: URLSendMail + "/SendMail",
          method: "POST",
          data: {
            strMailFrom: strEmail,
            strMailTo: $scope.MailTo.text,
            strMailBcc: $scope.MailBCC.text,
            strMailCC: $scope.MailCC.text,
            strSubject: $scope.MailSubject.text,
            strBody: $scope.MailDetail.text,
            isBodyHtml: "true",
            strURLFile: ""
          }
          /*data: { 'strMailFrom': $scope.MailFrom.text,'strMailTo':$scope.MailTo.text,'strMailBcc':$scope.MailBCC.text,'strMailCC':$scope.MailCC.text,'strSubject':$scope.MailSubject.text,'strBody':$scope.MailDetail.text,'isBodyHtml':"true",'strURLFile':$scope.FPFN}*/
        }).then(
          function(response) {
            alert("Send Mail Complete");
          },
          function(response) {
            if (response.status == 401) {
              TMLTHService.unauthorizedService();
            } else {
              alert("Send Mail Fail");
              alert(response.data.Message);
            }
          }
        );
      } catch (err) {
        alert("Error : " + err.message);
      }
    }
    $scope.MailFrom_Modal.text = "";
    $scope.MailTo_Modal.text = "";
    $scope.MailCC_Modal.text = "";
    $scope.MailBCC_Modal.text = "";

    $scope.MailSubject_Modal.text = "";
    $scope.MailDetail_Modal.text = "";

    $scope.MailTo_Modal.text = varMailFrom; //strEmail; //"aass"

    $scope.closeModal();
  };

  /*Chk format Email*/

  function ValidateEmailFormat(strEmail) {
    var email = strEmail;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      return false;
    } else {
      return true;
    }
  }

  $scope.ValidateMailFromFormat = function(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      $scope.MailFromMsg = "";
    } else {
      $scope.MailFromMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailToFormat = function(strEmail) {
    $scope.MailToMsg = "";
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailToMsg = "";
    } else {
      $scope.MailToMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailCCFormat = function(strEmail) {
    $scope.MailCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailCCMsg = "";
    } else {
      $scope.MailCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  $scope.ValidateMailBCCFormat = function(strEmail) {
    $scope.MailBCCMsg = "";
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }
    if (bl == true) {
      $scope.MailBCCMsg = "";
    } else {
      $scope.MailBCCMsg = "Email ไม่ถูกรูปแบบ";
    }
  };

  function ValidateMailFrom(strEmail) {
    if (ValidateEmailFormat(strEmail)) {
      //alert("T");
      return true;
    } else {
      //alert("F");
      return false;
    }
  }
  function ValidateMailTo(strEmail) {
    var res = strEmail.split(";");

    var bl = true;

    if (strEmail == "") {
      bl = false;
    }

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  function ValidateMailCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;

    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  function ValidateMailBCC(strEmail) {
    if (strEmail == undefined || strEmail == "") {
      return true;
    }
    var res = strEmail.split(";");

    var bl = true;
    for (i = 0; i < res.length; i++) {
      if (res[i] != "") {
        if (ValidateEmailFormat(res[i]) == false) {
          bl = false;
        }
      }
    }

    return bl;
  }
  /*End Chk format Email*/

  //////////////////////////////////////

  function SendMailContact() {
    try {
      $scope.MailAttached = "";

      //$scope.AttachedFile = { "File" : $scope.FPFN };

      $scope.MailFromMsg = "";
      $scope.MailToMsg = "";
      $scope.MailCCMsg = "";
      $scope.MailBCCMsg = "";

      $ionicModal
        .fromTemplateUrl("ModalSendMailDownload.tmpl.html", {
          scope: $scope,
          animation: "slide-in-up"
        })
        .then(function(modal) {
          $scope.modal = modal;

          $scope.openModal = function() {
            $scope.modal.show();
          };

          $scope.closeModal = function() {
            $scope.modal.hide();
          };

          $scope.$on("$destroy", function() {
            //$scope.modal.remove();
          });

          $scope.openModal();
        });
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  $scope.SendMailContactUs = function() {
    $scope.MailSubject_Modal.text = "";

    SendMailContact();
  };
});
