CPApp.controller("ManualCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  $ionicLoading,
  $ionicSlideBoxDelegate
) {
  //console.log("ManualCtrl");

  /*$scope.ListManual = [
    {
      id: 1,
      ManualHeader: "การลงทะเบียน",
      ManualImgHeaderPathFileName: "Resources/images/News/1.jpg",
      ManualDetail: "<p>เริ่มต้นจาก...</p>",
    },
    {
      id: 2,
      ManualHeader: "ลืมรหัสผ่าน",
      ManualImgHeaderPathFileName: "Resources/images/News/2.jpg",
      ManualDetail: "<p>อันดับแรก...</p>",
    },
    {
      id: 3,
      ManualHeader: "....",
      ManualImgHeaderPathFileName: "Resources/images/News/3.jpg",
      ManualDetail: "<p>...</p>",
    },
    {
      id: 4,
      ManualHeader: "...",
      ManualImgHeaderPathFileName: "Resources/images/News/4.jpg",
      ManualDetail: "<p>...</p>",
    }

  ]

  //Param To News Detail
  //$scope.id = $stateParams.id;
  $scope.IsFirstTime = 1;
*/

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $timeout(function() {
    $ionicLoading.hide();
  }, 2000);

  //alert("Register Manual");

  // Called to navigate to the main app
  $scope.startApp = function() {
    //alert('Go');
    //$state.go('AgentLogin');
    $state.go("app.Home");
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
  };
});
