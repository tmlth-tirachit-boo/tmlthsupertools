var CPApp = angular.module("ionicApp", ["ionic", "angular.filter"]);
CPApp.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {});
});
CPApp.config(function(
  $stateProvider,
  $urlRouterProvider,
  $ionicConfigProvider,
  $httpProvider
) {
  // PEN-TEST :: START ADD HTTP HEADER
  $httpProvider.defaults.headers.common.Authorization = "Bearer ,Basic ";
  // PEN-TEST :: END ADD HTTP HEADER
  $ionicConfigProvider.tabs.position("top");

  //$ionicConfigProvider.views.transition('android');

  document.addEventListener("deviceready", checkConnection, false);

  //checkConnection();
  function checkConnection() {
    var IsOnline = navigator.onLine;

    //alert(IsOnline);

    if (IsOnline == false) {
      alert("การเชื่อมต่อผิดพลาด กรุณาตรวจสอบอีกครั้ง");
      ionic.Platform.exitApp();
      navigator.app.exitApp();
    }
  }

  $stateProvider
    .state("app", {
      cache: false,
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.tmpl.html",
      controller: "MenuCtrl"
    })

    .state("OTP", {
      cache: false,
      url:
        "/OTP/{pIDCard}/{pClntnum}/{pChdrnum}/{pName_ins}/{pLastname_ins}/{pMobile_no}/{pPhone}/{pEmail}/{pUserid}/{pPwd}/{pAction}",
      //url: "/OTP/{pIDCard}/{pChdrnum}",
      templateUrl: "templates/OTP.tmpl.html",
      controller: "OTPCtrl"
    })

    .state("Register", {
      cache: false,
      url: "/Register",
      templateUrl: "templates/Register.tmpl.html",
      controller: "RegisterCtrl"
    })

    .state("RegisterManual", {
      cache: false,
      url: "/RegisterManual",
      templateUrl: "templates/RegisterManual.tmpl.html",
      controller: "RegisterManualCtrl"
    })

    .state("Message", {
      cache: false,
      url: "/Message",
      templateUrl: "templates/Message.tmpl.html",
      controller: "RegisterManualCtrl"
    })

    .state("app.ProfileSetting", {
      cache: false,
      url: "/ProfileSetting",
      views: {
        menuContent: {
          templateUrl: "templates/ProfileSetting.tmpl.html",
          controller: "ProfileSettingCtrl"
        }
      }
    })

    /*.state('Login', {
         cache: false,
    url: "/Login",
    templateUrl: "templates/Login.tmpl.html",
    controller: 'AuthorizationCtrl'
  })*/
    .state("AgentLogin", {
      cache: false,
      url: "/AgentLogin",
      templateUrl: "templates/Login.tmpl.html",
      controller: "AgentAuthorizationCtrl"
    })

    .state("Logout", {
      cache: false,
      url: "/AgentLogin",
      templateUrl: "templates/Login.tmpl.html",
      controller: "AgentAuthorizationCtrl"
    })

    .state("Forgot", {
      cache: false,
      url: "/Forgot",
      templateUrl: "templates/Forgot.tmpl.html",
      controller: "ForgotPWDCtrl"
    })

    .state("CreateNewPWD", {
      cache: false,
      url: "/CreateNewPWD/{pIDCard}/{pOTP}/{pOTP_ID}",
      templateUrl: "templates/CreateNewPWD.tmpl.html",
      controller: "CreateNewPWDCtrl"
    })

    .state("Manual", {
      cache: false,
      url: "/Manual",
      templateUrl: "templates/Manual.tmpl.html",
      controller: "ManualCtrl"
    })

    .state("app.Home", {
      cache: false,
      url: "/Home",
      views: {
        menuContent: {
          templateUrl: "templates/Home.tmpl.html",
          controller: "HomeCtrl"
        }
      }
    })

    .state("app.CustomerList", {
      cache: false,
      url: "/CustomerList/{ascode}",
      views: {
        menuContent: {
          templateUrl: "templates/CustomerList.tmpl.html",
          controller: "CustomerListCtrl"
        }
      }
    })

    .state("app.UnitList", {
      cache: false,
      url: "/UnitList/{ascode}",
      views: {
        menuContent: {
          templateUrl: "templates/UnitList.tmpl.html",
          controller: "UnitListCtrl"
        }
      }
    })

    .state("app.AgentList", {
      cache: false,
      url: "/AgentList/{unitco}",
      views: {
        menuContent: {
          templateUrl: "templates/AgentList.tmpl.html",
          controller: "AgentListCtrl"
        }
      }
    })

    // Add :: Fail Direct Debit and Credit Card Report
    .state("app.FailRptCustomerList", {
      cache: false,
      url: "/FailRptCustomerList/{ascode}",
      views: {
        menuContent: {
          templateUrl: "templates/FailRptCustomerList.tmpl.html",
          controller: "FailRptCustomerListCtrl"
        }
      }
    })

    .state("app.FailRptUnitList", {
      cache: false,
      url: "/FailRptUnitList/{ascode}",
      views: {
        menuContent: {
          templateUrl: "templates/FailRptUnitList.tmpl.html",
          controller: "FailRptUnitListCtrl"
        }
      }
    })

    .state("app.FailRptAgentList", {
      cache: false,
      url: "/FailRptAgentList/{unitco}",
      views: {
        menuContent: {
          templateUrl: "templates/FailRptAgentList.tmpl.html",
          controller: "FailRptAgentListCtrl"
        }
      }
    })

    .state("app.FailRptCustomerDetail", {
      cache: false,
      url: "/FailRptCustomerDetail/{Chdrnum}/{Ascode}",
      views: {
        menuContent: {
          templateUrl: "templates/FailRptCustomerDetail.tmpl.html",
          controller: "FailRptCustomerDetailCtrl"
        }
      }
    })

    .state("app.InsureInfoVw", {
      cache: false,
      url: "/InsureInfoVw/{clntNo}",
      views: {
        menuContent: {
          templateUrl: "templates/InsureInfoVw.tmpl.html",
          controller: "InsureInfoVwCtrl"
        }
      }
    })

    .state("app.InsureInfoVw.InsuranceCardVw", {
      cache: false,
      url: "/InsuranceCardVw",
      views: {
        InsuranceCardVw: {
          templateUrl: "templates/InsureInfoVw-InsuranceCardVw.tmpl.html"
        }
      }
    })

    .state("app.InsureInfoVw.InsuranceInfoVw", {
      cache: false,
      url: "/InsuranceInfoVw",
      views: {
        InsuranceInfoVw: {
          templateUrl: "templates/InsureInfoVw-InsuranceInfoVw.tmpl.html"
        }
      }
    })

    .state("app.InsureInfoVw.ClaimVw", {
      cache: false,
      url: "/ClaimVw",
      views: {
        ClaimVw: {
          templateUrl: "templates/InsureInfoVw-ClaimVw.tmpl.html"
        }
      }
    })

    /*V4*/
    .state("app.ClaimDetailVw", {
      cache: false,
      url: "/ClaimDetailVw/{id}/{Policy_no}/{IncidentDT}/{rgpnum}",
      views: {
        menuContent: {
          templateUrl: "templates/InsureInfoVw-ClaimDetailVw.tmpl.html",
          controller: "InsureInfoVwCtrl"
        }
      }
    })

    .state("app.UnitLink", {
      cache: false,
      url: "/UnitLink/{chdrnum}/{product_code}",
      views: {
        menuContent: {
          templateUrl: "templates/UnitLink.tmpl.html",
          controller: "UnitLinkCtrl"
        }
      }
    })

    .state("app.UnitLinkDetail", {
      cache: false,
      url:
        "/UnitLinkDetail/{chdrnum}/{date_from}/{date_to}/{coverage}/{fund_code}",
      views: {
        menuContent: {
          templateUrl: "templates/UnitLinkDetail.tmpl.html",
          controller: "UnitLinkDetailCtrl"
        }
      }
    })

    .state("app.PaymentLogVw", {
      cache: false,
      url: "/PaymentLogVw",
      views: {
        menuContent: {
          templateUrl: "templates/PaymentLogVw.tmpl.html",
          controller: "PaymentLogVwCtrl"
        }
      }
    })

    .state("app.Notification", {
      cache: false,
      url: "/Notification",
      views: {
        menuContent: {
          templateUrl: "templates/Notification.tmpl.html",
          controller: "NotificationCtrl"
        }
      }
    })

    // .state("app.InsureInfo", {
    //   cache: false,
    //   url: "/InsureInfo",
    //   abstract: true,
    //   views: {
    //     menuContent: {
    //       templateUrl: "templates/InsureInfo.tmpl.html",
    //       controller: "InsureInfoCtrl"
    //     }
    //   }
    // })
    // .state("app.InsureInfo.InsuranceCard", {
    //   cache: false,
    //   url: "/InsuranceCard",
    //   views: {
    //     InsuranceCard: {
    //       templateUrl: "templates/InsureInfo-InsuranceCard.tmpl.html"
    //     }
    //   }
    // })
    // .state("app.InsureInfo.InsuranceInfo", {
    //   cache: false,
    //   url: "/InsuranceInfo",
    //   views: {
    //     InsuranceInfo: {
    //       templateUrl: "templates/InsureInfo-InsuranceInfo.tmpl.html"
    //     }
    //   }
    // })
    // .state("app.InsureInfo.Claim", {
    //   cache: false,
    //   url: "/Claim",
    //   views: {
    //     Claim: {
    //       templateUrl: "templates/InsureInfo-Claim.tmpl.html"
    //     }
    //   }
    // })

    // .state("app.ClaimDetail", {
    //   cache: false,
    //   url: "/ClaimDetail/{id}/{Policy_no}",
    //   views: {
    //     menuContent: {
    //       templateUrl: "templates/InsureInfo-ClaimDetail.tmpl.html",
    //       controller: "InsureInfoCtrl"
    //     }
    //   }
    // })

    // .state("app.PaymentLog", {
    //   cache: false,
    //   url: "/PaymentLog",
    //   views: {
    //     menuContent: {
    //       templateUrl: "templates/PaymentLog.tmpl.html",
    //       controller: "PaymentLogCtrl"
    //     }
    //   }
    // })

    .state("app.Hospital", {
      cache: false,
      url: "/Hospital",
      views: {
        menuContent: {
          templateUrl: "templates/Hospital.tmpl.html",
          controller: "HospitalCtrl"
        }
      }
    })

    .state("app.DownloadDocument", {
      cache: false,
      url: "/DownloadDocument",
      views: {
        menuContent: {
          templateUrl: "templates/DownloadDocument.tmpl.html",
          controller: "DownloadCtrl"
        }
      }
    })

    .state("app.DownloadDetail", {
      cache: false,
      url: "/DownloadDetail/{idx}",
      views: {
        menuContent: {
          templateUrl: "templates/DownloadDetail.tmpl.html",
          controller: "DownloadCtrl"
        }
      }
    })

    .state("app.ContactUs", {
      cache: false,
      url: "/ContactUs",
      views: {
        menuContent: {
          templateUrl: "templates/ContactUs.tmpl.html",
          controller: "ContactUsCtrl"
        }
      }
    })

    .state("app.News", {
      cache: false,
      url: "/News",
      views: {
        menuContent: {
          templateUrl: "templates/News.tmpl.html",
          controller: "NewsCtrl"
        }
      }
    })

    .state("app.NewsDetail", {
      cache: false,
      url: "/NewsDetail/{id}",
      views: {
        menuContent: {
          templateUrl: "templates/NewsDetail.tmpl.html",
          controller: "NewsCtrl"
        }
      }
    })

    .state("app.Branch", {
      cache: false,
      url: "/Branch",
      views: {
        menuContent: {
          templateUrl: "templates/Branch.tmpl.html",
          controller: "BranchCtrl"
        }
      }
    })

    .state("app.PDFViewer", {
      cache: false,
      url: "/PDFViewer",
      views: {
        menuContent: {
          templateUrl: "templates/PDFViewer.tmpl.html",
          controller: "PDFCtrl"
        }
      }
    })

    .state("app.Map", {
      cache: false,
      url: "/Map/{lat}/{lng}",
      views: {
        menuContent: {
          templateUrl: "templates/Map.tmpl.html",
          controller: "MapCtrl"
        }
      }
    });

  //$urlRouterProvider.otherwise("/app/Home");
  ////$ionicConfigProvider.views.maxCache(0);

  $urlRouterProvider.otherwise("/AgentLogin");
});

// START :: PEN-TEST ADD MANAGE TOKEN
// function TokenManager() {}
// TokenManager.SetBasic = function(value) {
//   window.localStorage.setItem("BasicToken", value);
// };
// TokenManager.GetBasic = function() {
//   var BasicToken = window.localStorage.getItem("BasicToken");
//   if (BasicToken == null || BasicToken == undefined) {
//     return "";
//   } else {
//     return BasicToken;
//   }
// };
// TokenManager.DelBasic = function() {
//   window.localStorage.removeItem("BasicToken");
// };

// TokenManager.Set = function(value) {
//   window.localStorage.setItem("UserToken", value);
// };
// TokenManager.Get = function() {
//   var UserToken = window.localStorage.getItem("UserToken");
//   if (UserToken == null || UserToken == undefined) {
//     return "";
//   } else {
//     return UserToken;
//   }
// };
// TokenManager.Del = function() {
//   window.localStorage.removeItem("UserToken");
// };

CPApp.service("JWTService", function() {
  let BasicToken = "";
  let UserToken = "";

  return {
    GetBasic: function() {
      if (BasicToken == null || BasicToken == undefined) {
        return "";
      } else {
        return BasicToken;
      }
    },
    SetBasic: function(value) {
      BasicToken = value;
    },
    DelBasic: function() {
      BasicToken = "";
    },
    Get: function() {
      if (UserToken == null || UserToken == undefined) {
        return "";
      } else {
        return UserToken;
      }
    },
    Set: function(value) {
      UserToken = value;
    },
    Del: function() {
      UserToken = "";
    }
  };
});
// END :: PEN-TEST ADD MANAGE TOKEN

CPApp.service("TMLTHService", function($http, $state, JWTService) {
  this.unauthorizedService = function() {
    // Call service LogOut for delete Token on server
    try {
      $http({
        url: URLAG + "/LogOut",
        method: "POST",
        data: {}
      }).then(
        function(response) {
          // Success
        },
        function(response) {
          // Fail
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }

    // Delete Token on local storage
    JWTService.DelBasic();
    JWTService.Del();

    // Reset http default header
    $http.defaults.headers.common.Authorization =
      "Bearer" +
      " " +
      JWTService.Get() +
      ",Basic" +
      " " +
      JWTService.GetBasic();

    $state.go("AgentLogin", {}, { reload: true });
  };
});

// CPApp.factory("myInterceptor", [
//   function($state) {
//     var myInterceptor = {
//       responseError: function(rejection) {
//         console.log(rejection.status);
//         if (rejection.status == 401) {
//           //$state.go("Login", {}, { reload: true });
//         }
//       }
//     };

//     return myInterceptor;
//   }
// ]);
