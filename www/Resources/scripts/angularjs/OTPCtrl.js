CPApp.controller("OTPCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  TMLTHService
) {
  // console.log("OTPCtrl");

  $scope.isDisabled = false;
  $scope.disabled = false;

  //Param To News Detail
  $scope.id = $stateParams.id;
  $scope.pIDCard = $stateParams.pIDCard;
  $scope.pChdrnum = $stateParams.pChdrnum;
  $scope.pName_ins = $stateParams.pName_ins;
  $scope.pLastname_ins = $stateParams.pLastname_ins;
  $scope.pMobile_no = $stateParams.pMobile_no;
  $scope.pPhone = $stateParams.pPhone;
  $scope.pEmail = $stateParams.pEmail;
  $scope.pUserid = $stateParams.pUserid;
  $scope.pPwd = $stateParams.pPwd;

  $scope.pAction = $stateParams.pAction;

  $scope.Clntnum = $stateParams.pClntnum;

  $scope.Otp = "";

  function ConvertMobilePhone(MobilePhoneVal) {
    var tmp = MobilePhoneVal.substring(1);

    var NewMobileval = "66" + tmp;

    return NewMobileval;
  }

  /*Check Dupplicate UserID*/
  $scope.RequestOTP = function() {
    //alert($scope.pAction);

    $scope.disabled = true;

    $timeout(function() {
      //alert('test');
      $scope.disabled = false;
    }, 10000);

    if ($scope.pAction == ActRegister) {
      RequestOTPRegister();
    } else if ($scope.pAction == ActForgotPWD) {
      RequestOTPForgotPWD();
    }
  };

  function RequestOTPRegister() {
    alert("ระบบจะส่งรหัส\r\nไปยังเบอร์มือถือที่ลงทะเบียนไว้");
    $scope.isDisabled = true;

    try {
      $http({
        url: URLOtp + "/Request_OTP",
        method: "POST",
        data: {
          Mobile: ConvertMobilePhone($scope.pMobile_no),
          RequestBy: $scope.pIDCard,
          Key: $scope.pAction,
          Ref1: $scope.pChdrnum,
          Ref2: ""
        }
      }).then(
        function(response) {},
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function RequestOTPForgotPWD() {
    alert("ระบบจะส่งรหัส\r\nไปยังเบอร์มือถือที่ลงทะเบียนไว้");
    $scope.isDisabled = true;

    try {
      $http({
        url: URLOtp + "/Request_OTP",
        method: "POST",
        data: {
          Mobile: ConvertMobilePhone($scope.pMobile_no),
          RequestBy: $scope.pIDCard,
          Key: $scope.pAction,
          Ref1: $scope.pIDCard,
          Ref2: ""
        }
      }).then(
        function(response) {},
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  // $scope.RequestOTPRegister = function() {
  // alert('ระบบจะส่งรหัส\r\nไปยังเบอร์มือถือที่ลงทะเบียนไว้');

  // try {
  // $http({
  // url: URLOtp + "/Request_OTP",
  // method: "POST",
  // data: { 'Mobile': $scope.pMobile_no,'RequestBy': $scope.pIDCard,'Key':$scope.pAction,'Ref1':$scope.pChdrnum,'Ref2':''}
  // //data: { 'Mobile': $scope.pMobile_no,'AppName': AppName,'RequestBy': $scope.pIDCard,'Ref1':$scope.pChdrnum,'Ref2':'','Action': $scope.pAction}
  // })
  // .then(function(response) {

  // //alert('Success');
  // //debugger;
  // //$scope.ListHospital = response.data.d;
  // //alert(response.data.d);
  // // success
  // //if(response.data.d == false){
  // //  $scope.DupUserIDMsg = '';
  // //  $scope.CanSubmit = true;
  // //  }else{
  // //    $scope.DupUserIDMsg='ชื่อผู้ใช้งานนี้ถูกใช้งานแล้ว';
  // //    $scope.CanSubmit = false;
  // //  }

  // },
  // function(response) { // optional
  // // failed
  // alert('Fail');
  // //debugger;
  // });

  // } catch (err) {
  // alert('Error : ' + err.message);
  // }

  // }
  /*End Check Dupplicate UserID*/

  $scope.CancelOTP = function() {
    $state.go("Login", {}, { reload: true });
  };

  $scope.Submit = function(OtpVal) {
    if ($scope.pAction == ActRegister) {
      SubmitRegister(OtpVal);
    } else if ($scope.pAction == ActForgotPWD) {
      SubmitForgotPWD(OtpVal);
    }
  };

  function SubmitRegister(OtpVal) {
    try {
      $http({
        url: URLOtp + "/Enqurie",
        method: "POST",
        data: {
          Mobile: ConvertMobilePhone($scope.pMobile_no),
          Key: $scope.pAction,
          OTP: OtpVal,
          Ref1: $scope.pChdrnum,
          Ref2: ""
        }
      }).then(
        function(response) {
          if (response.data.d == "VALID") {
            SaveUser();
          } else {
            alert(response.data.d);
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  //Insert : Register
  //Update : Edit Profile, Change Password
  function SaveUser() {
    try {
      $http({
        url: URLRegister + "/Register",
        method: "POST",
        data: {
          strIDCard: $scope.pIDCard,
          strClntnum: $scope.Clntnum,
          strChdrnum: $scope.pChdrnum,
          strName_ins: $scope.pName_ins,
          strLastname_ins: $scope.pLastname_ins,
          strMobile_no: $scope.pMobile_no,
          strPhone: $scope.pPhone,
          strEmail: $scope.pEmail,
          strUserid: $scope.pUserid,
          strPassword: $scope.pPwd
        }
      }).then(
        function(response) {
          if (response.data.d == true) {
            alert("บันทึกเรียบร้อย");
            $state.go("Login", {}, { reload: true });
          } else {
            alert("บันทึกข้อมูลล้มเหลว");
            return;
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function SubmitForgotPWD(OtpVal) {
    //alert(OtpVal);
    try {
      $http({
        url: URLOtp + "/Enqurie",
        method: "POST",
        data: {
          Mobile: ConvertMobilePhone($scope.pMobile_no),
          Key: $scope.pAction,
          OTP: OtpVal,
          Ref1: $scope.pIDCard,
          Ref2: ""
        }
      }).then(
        function(response) {
          if (response.data.d == "VALID") {
            fnCreateNewPWD(OtpVal);
          } else {
            alert(response.data.d);
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }

  function fnCreateNewPWD(OtpVal) {
    try {
      $http({
        url: URLOtp + "/Enqurie_OTP_Detail",
        method: "POST",
        data: {
          Mobile: ConvertMobilePhone($scope.pMobile_no),
          Key: $scope.pAction,
          OTP: OtpVal,
          Ref1: $scope.pIDCard,
          Ref2: ""
        }
      }).then(
        function(response) {
          $scope.guidOTPID = response.data.d[0].ID;
          $state.go("CreateNewPWD", {
            pIDCard: $scope.pIDCard,
            pOTP: OtpVal,
            pOTP_ID: $scope.guidOTPID
          });
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  }
});
