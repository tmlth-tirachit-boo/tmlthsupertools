CPApp.controller("ProfileSettingCtrl", function(
  $scope,
  $http,
  $ionicLoading,
  TMLTHService,
  EncryptDecryptData
) {
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });
  try {
    // debugger;
    $http({
      url: URLAG + "/GetAgentDetail",
      method: "POST",
      data: {
        strAsCode: EncryptDecryptData.Encrypt(strASCode)
      }
    }).then(
      function(response) {
        $ionicLoading.hide();

        $scope.Email = response.data.d[0].Email;
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert(response.data.Message);
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  $scope.saveData = input_email => {
    

    if (input_email == "") {
      alert("กรุณากรอกข้อมูลให้ครบ");
    } else {
      var email = input_email;
      var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if (!filter.test(email)) {
        alert("รูปแบบอีเมลไม่ถูกต้อง");
      } else {
        $ionicLoading.show({
          content: "Loading",
          animation: "fade-in",
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0
        });
        try {
          // debugger;
          $http({
            url: URLAG + "/SetAgentUser",
            method: "POST",
            data: {
              strAsCode: EncryptDecryptData.Encrypt(strASCode),
              strEmail: EncryptDecryptData.Encrypt(input_email)
            }
          }).then(
            function(response) {
              $ionicLoading.hide();
              alert("บันทึกสำเร็จ");
            },
            function(response) {
              if (response.status == 401) {
                TMLTHService.unauthorizedService();
              } else {
                alert(response.data.Message);
              }
            }
          );
        } catch (err) {
          alert("Error : " + err.message);
        }
      }
    }
  };
});
