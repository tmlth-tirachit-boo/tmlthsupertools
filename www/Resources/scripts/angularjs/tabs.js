angular.module('ionicApp', ['ionic'])

.config(function($stateProvider, $urlRouterProvider) {

$stateProvider

	.state('tab', {
 		url: "/tab",
 		abstract: true,
 	})

  .state('home', {
  	url: '/home',
  	views: {
    home: {
      templateUrl: 'home.html'
    }
  }
})

 .state('help', {
  url: '/help',
  views: {
    help: {
      templateUrl: 'help.html'
    }
  }
})

.state('contact', {
  url: '/contact',
  views: {
    contact: {
      templateUrl: 'contact.html'
    }
  }
});


   $urlRouterProvider.otherwise("tab.home");

})

.controller('HomeTabCtrl', function($scope) {
  // console.log('HomeTabCtrl');
});
