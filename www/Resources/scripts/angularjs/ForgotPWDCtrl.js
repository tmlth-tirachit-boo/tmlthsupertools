var ActionValForgot = ActForgotPWD;

CPApp.controller("ForgotPWDCtrl", function(
  $scope,
  $http,
  $state,
  TMLTHService
) {
  // console.log("ForgotPWDCtrl");

  $scope.CanSubmit = true;
  $scope.IDCard = "";
  $scope.Email = "";
  $scope.Mobile_no = "";

  /*Chk Format ID Card*/
  function checkID(id) {
    if (id.length != 13) return false;
    for (i = 0, sum = 0; i < 12; i++)
      sum += parseFloat(id.charAt(i)) * (13 - i);
    if ((11 - (sum % 11)) % 10 != parseFloat(id.charAt(12))) return false;
    return true;
  }
  function CheckFormatIDCard(IDCardVal) {
    if (!checkID(IDCardVal)) return false;
    else return true;
  }
  /*End Chk Format ID Card*/

  /*Check Dupplicate IDCard*/
  $scope.IsIDCard = function(valIDCard) {
    //alert(valIDCard);
    if (valIDCard == undefined) {
      return;
    }

    /*IDCard*/
    if (valIDCard.length != 13) {
      //alert('หมายเลขบัตรประชาชน ต้องมี 13 หลัก ');
      $scope.DupIDCarsMsg = "หมายเลขบัตรประชาชน ต้องมี 13 หลัก";
      $scope.CanSubmit = false;
      return false;
    } else {
      if (CheckFormatIDCard(valIDCard) == false) {
        //alert('รูปแบบของหมายเลขบัตรไม่ถูกต้อง');
        $scope.DupIDCarsMsg = "รูปแบบของหมายเลขบัตรไม่ถูกต้อง";
        $scope.CanSubmit = false;
        return false;
      } else {
        $scope.DupIDCarsMsg = "";
        $scope.CanSubmit = true;
      }
    }
  };
  /*End Check Dupplicate IDCard*/

  /*Chk format Email*/
  $scope.ValidateEmailFormat = function checkEmail(strEmail) {
    var email = strEmail;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      //alert('Please provide a valid email address');
      //email.focus;
      $scope.InvalidEmailFormatMsg = "Email ไม่ถูกรูปแบบ";
      $scope.CanSubmit = false;
      return false;
    } else {
      $scope.InvalidEmailFormatMsg = "";
      $scope.CanSubmit = true;
      return true;
    }
  };
  /*End Chk format Email*/

  /*Chk Mobile No*/
  $scope.ValidateMobileNo = function checkMobileNo(strMobileNo) {
    //alert(strMobileNo);
    if (strMobileNo.trim() == "") {
      $scope.InvalidMobileNoMsg = "กรุณาระบุหมายเลขโทรศัพท์";
      $scope.CanSubmit = false;
      return false;
    } else {
      $scope.InvalidMobileNoMsg = "";
      $scope.CanSubmit = true;
      return true;
    }
  };
  /*End Chk Mobile No*/

  $scope.ForgotPWD = function(IDCardVal, EmailVal, Mobile_noVal) {
    if ($scope.CanSubmit == false) {
      alert("ไม่สามารถดำเนินการได้ กรุณาตรวจสอบข้อมูลอีกครั้ง");
      return;
    }

    if (ValidateForgotPWD(IDCardVal, EmailVal, Mobile_noVal) == true) {
      if (IsRegisteredData(IDCardVal, EmailVal, Mobile_noVal) == false) {
        return;
      }
    } else {
      alert("ไม่สามารถดำเนินการได้ กรุณาตรวจสอบข้อมูลอีกครั้ง");
      return;
    }
  };

  function ValidateForgotPWD(IDCardVal, EmailVal, Mobile_noVal) {
    //alert(Mobile_noVal);

    /*Required Field*/
    if (IDCardVal == "" || Mobile_noVal == "" || EmailVal == "") {
      alert("กรุณากรอกข้อมูลให้ครบ");
      return false;
    }

    /*IDCard*/
    if (IDCardVal.length != 13) {
      //alert('หมายเลขบัตรประชาชน ต้องมี 13 หลัก ');
      return false;
    } else {
      if (CheckFormatIDCard(IDCardVal) == false) {
        //alert('รูปแบบของหมายเลขบัตรไม่ถูกต้อง');
        return false;
      }
    }

    var email = EmailVal;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
      return false;
    }

    return true;
  }

  function IsRegisteredData(IDCardVal, EmailVal, Mobile_noVal) {
    try {
      $http({
        url: URLRegister + "/IsRegisteredData",
        method: "POST",
        data: {
          strIDCard: IDCardVal,
          strEmail: EmailVal,
          strMobile_no: Mobile_noVal
        }
      }).then(
        function(response) {
          if (response.data.d == false) {
            alert("ไม่พบข้อมูลผู้ลงทะเบียนดังกล่าว");
            return false;
          } else {
            $state.go("OTP", {
              pIDCard: IDCardVal,
              pEmail: EmailVal,
              pMobile_no: Mobile_noVal,
              pAction: ActionValForgot
            });
            //return true;
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Error");
          }
          return false;
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
      return false;
    }
  }

  $scope.GoToLogin = function() {
    $state.go("Login", {}, { reload: true });
  };

  // $scope.TmpOTP=function()
  // {
  //   //alert('click');
  //   //$state.go('OTP', {pIDCard: '555'}, { reload: true });

  //     $state.go('OTP', {pIDCard: '8899',pChdrnum:'5566',pName_ins:'JK',pLastname_ins:'KPS',pMobile_no:'0844259888',pPhone:'-',pEmail:'JK@gmail.com',pUserid:'JKuser',pPwd:'1234',pAction:'registration'});
  //     //$state.go('OTP', {pIDCard: '8899',pChdrnum:'5566'});
  // }
});
