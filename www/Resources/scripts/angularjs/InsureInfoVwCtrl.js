CPApp.controller("InsureInfoVwCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  $ionicLoading,
  TMLTHService,
  EncryptDecryptData
) {
  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $scope.BaseInfoOccdate = "-";
  $scope.BaseInfoBillfreq = "-";
  $scope.BaseInfoPtdate = "-";
  $scope.BaseInfoAgnt_name = "-";
  $scope.BaseInfoListTel = "-";
  $scope.BaseInfoBasic_instprem = "-";

  $scope.TestNa = "";
  $scope.TestNa = "";

  $scope.ddlSelectChdrnum = ""; //Insure ID
  $scope.GetInsuranceInfoByChdrnum = function(val) {
    setTimeout(function() {
      fnGetInsuranceInfoByChdrnum(val);
    }, 3000);

    fnGetInsureCardInfo("");
  };

  $scope.openPayment = chdrnum => {
    const payment_url = `${URLCommonCardWebsite}?id=${encodeURI(chdrnum)}`
    var ref = null;
    // console.log(chdrnum);
    // console.log(encodeURI(chdrnum));
    // console.log(payment_url);
    if (ionic.Platform.platform() == "ios") {
      ref = cordova.InAppBrowser.open(
        payment_url,
        "_system",
        "usewkwebview=yes,hidden=yes,location=no,hideurlbar=yes,closebuttoncaption=ปิด"
      );
    } else {
      ref = cordova.InAppBrowser.open(
        payment_url,
        "_system",
        "hidden=yes,location=no,hideurlbar=yes,closebuttoncaption=ปิด,footer=yes"
      );
    }

    ref.show();
  };

  //Param To News Detail
  $scope.id = $stateParams.id;
  $scope.Policy_no = $stateParams.Policy_no;
  $scope.IncidentDT = $stateParams.IncidentDT;
  $scope.rgpnum = $stateParams.rgpnum;

  // Add by Tirachit B. @20170705 //
  $scope.stringNullReturnHyphen = function(x) {
    //alert(x);
    if (x == null || x == "") {
      return "-";
    } else {
      return x;
    }
  };
  // End Add by Tirachit B. @20170705 //

  /* common */
  $scope.numberWithCommas = function(x) {
    if (x == null) {
      return "-";
    }

    //var objNum = String(x).replace(',','');
    //var objNum = x.replace(/,/g, '');
    var objNum = String(x).replace(/,/g, "");
    //alert(objNum);
    var Num = parseFloat(objNum).toFixed(2) || 0;
    //alert(Num);

    var parts = Num.toString().split(".");
    //alert(parts);
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //alert(parts[0]);
    //alert(parts.join("."));
    return parts.join(".");
  };

  function fnNumberWithCommas(x) {
    //alert(x);
    //alert(x.toFixed(2));

    if (x == null) {
      return "-";
    }

    //var objNum = String(x).replace(",","");
    var objNum = String(x).replace(/,/g, "");
    var Num = parseFloat(objNum).toFixed(2) || 0;

    var parts = objNum.toString().split(".");
    //alert(parts);
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //alert(parts[0]);
    //alert(parts.join("."));
    return parts.join(".");
  }

  $scope.ConvertJSONDateToStringTH = function(DateVal) {
    var dateString = DateVal.substr(6); //"\/Date(1334514600000)\/".substr(6);
    //alert(dateString);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var date = month + "/" + day + "/" + year;
    //alert(date);
    return getFormattedDateTH(date);
  };
  $scope.ConvertJSONDateToString = function(DateVal) {
    var dateString = DateVal.substr(6); //"\/Date(1334514600000)\/".substr(6);
    //alert(dateString);
    var currentTime = new Date(parseInt(dateString));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();

    var date = pad(day) + "/" + pad(month) + "/" + year;

    return date;
  };
  function fnConvertJSONDateToString(DateVal) {
    var dateString = DateVal.substr(6); //"\/Date(1334514600000)\/".substr(6);
    //alert(dateString);
    var currentTime = new Date(parseInt(dateString));

    //alert(currentTime);
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    //alert(year);
    var date = pad(day) + "/" + pad(month) + "/" + year;

    //return date;

    if (year == 1) {
      return "-";
    } else {
      return date;
    }

    //var date =  month + "/" + day  + "/" + year;
    //alert(date);
    //return getFormattedDateTH(date);
  }
  function pad(d) {
    return d < 10 ? "0" + d.toString() : d.toString();
  }

  $scope.ConvertYYYYMMDDDateToString = function(DateVal) {
    if (
      DateVal == null ||
      DateVal == "" ||
      DateVal == "99999999" ||
      DateVal == "-"
    ) {
      return "-";
    } else {
      //alert(DateVal);
      var sDate = DateVal.toString();
      var year = sDate.substr(0, 4);
      var month = sDate.substr(4, 2);
      var day = sDate.substr(6, 2);

      var date = month + "/" + day + "/" + (parseInt(year) + 543).toString();
      //alert(date);
      return getFormattedDateTH(date);
    }
  };

  function fnConvertYYYYMMDDDateToString(DateVal) {
    if (
      DateVal == null ||
      DateVal == "" ||
      DateVal == "99999999" ||
      DateVal == "-"
    ) {
      return "-";
    } else {
      //alert(DateVal);
      var sDate = DateVal.toString();
      var year = sDate.substr(0, 4);
      var month = sDate.substr(4, 2);
      var day = sDate.substr(6, 2);

      var date = month + "/" + day + "/" + (parseInt(year) + 543).toString();
      //alert(date);
      return getFormattedDateTH(date);
    }
  }

  function getFormattedDateEN(input) {
    if (input == null || input == "") {
      return "-";
    } else {
      var pattern = /(.*?)\/(.*?)\/(.*?)$/;
      var result = input.replace(pattern, function(match, p1, p2, p3) {
        var months = [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];
        return (p2 < 10 ? "" + p2 : p2) + " " + months[p1 - 1] + " " + p3;
      });

      return result;
    }
  }

  function getFormattedDateTH(input) {
    if (input == null || input == "") {
      return "-";
    } else {
      var pattern = /(.*?)\/(.*?)\/(.*?)$/;
      var result = input.replace(pattern, function(match, p1, p2, p3) {
        var months = [
          "ม.ค.",
          "ก.พ.",
          "มี.ค.",
          "เม.ย.",
          "พ.ค.",
          "มิ.ย.",
          "ก.ค.",
          "ส.ค.",
          "ก.ย.",
          "ต.ค.",
          "พ.ย.",
          "ธ.ค."
        ];
        return (p2 < 10 ? "" + p2 : p2) + " " + months[p1 - 1] + " " + p3;
      });

      return result;
    }
  }

  $scope.TransformDescGroupingClaim = function(DataVal) {
    var strCode = "";
    var strData = "";
    var strRet = "";

    strCode = DataVal.substring(0, DataVal.indexOf(":"));

    strData = DataVal.substring(DataVal.indexOf(":") + 1);

    strRet = "สัญญาเพิ่มเติม" + strData + "(" + strCode + ")";

    return strRet;
  };
  /*end common*/

  $scope.Clntnum = $stateParams.clntNo;
  if (strClntnumVw != null) {
    var strAsCodeSelected;
    if (strSelectedASCodeAgent != null && strSelectedASCodeAgent != "") {
      strAsCodeSelected = strSelectedASCodeAgent;
    } else {
      strAsCodeSelected = strASCode;
    }
    try {
      $http({
        url: URLInsure + "/GetInsuranceByClientNumberUnderAgent_New",
        method: "POST",
        data: {
          strClientNumber: EncryptDecryptData.Encrypt(strClntnumVw),
          strAgentNum: EncryptDecryptData.Encrypt(strAsCodeSelected)
        }
      }).then(
        function(response) {
          $scope.ListInsuranceNo = response.data.d;

          if (response.data.d != "") {
            $scope.DefaultListInsuranceNo = response.data.d[0].Chdrnum;
            fnGetInsuranceInfoByChdrnum($scope.DefaultListInsuranceNo);
            // Tirachit Add DefaultListInsuredName
            $scope.DefaultListInsuredName = response.data.d[0].Insured_name;
          } else {
            $timeout(function() {
              $ionicLoading.hide();
            }, 2000);
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert(
              "Fail : GetInsuranceByClientNumberUnderAgent_New" +
                response.message
            );
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  } else {
    fnGetInsureCardInfo($scope.Policy_no);
    $timeout(function() {
      $ionicLoading.hide();
    }, 2000);
  }

  function fnGetInsuranceInfoByChdrnum(val) {
    //val='11543252';
    //alert(val);
    //******************************//
    fnGetDataAsOf();

    /***************************************************************************/
    /*Insure Card*/
    fnGetInsureCardInfo(val);

    /***************************************************************************/
    /*Insure Base Info*/
    fnGetInsureBaseInfo(val);

    //val='10808114';
    /*********************************************************************/
    /*Claim*/
    //val='11333268'
    fnGetInsureClaim(val);
  }

  //************* Add fnGetDataAsOf by tirachit.boo *****************//
  function fnGetDataAsOf() {
    //alert("hi");
    try {
      $http({
        url: URLCommon + "/GetAppVersionService",
        method: "POST",
        data: { strAppID: AppID, strInputVer: AppVer }
      }).then(
        function(response) {
          $scope.latesdDataDate = response.data.d[0].AppLatestDataDateText;
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      // alert("Error GetAppVersionService: " + err.message);
    }
  }

  function fnGetInsureCardInfo(val) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    // debugger
    console.log("param");
    console.log(EncryptDecryptData.Encrypt(val));
    if (val !== "" && val !== null) {
      try {
        $http({
          //url: URLInsure + "/GetInsuranceCard_New",
          url: URLInsuredCard + "/GetInsuredCard",
          method: "POST",
          data: {
            strPolicyNo: EncryptDecryptData.Encrypt(val)
          }
        }).then(
          function(response) {
            // console.log(JSON.stringify(response.data.d));
            $scope.InsuranceCardInfo = "";
            const jsonResult = JSON.parse(response.data.d);
            console.log("card result");
            console.log(jsonResult.data);
            if (jsonResult.statusCode === 200) {
              $scope.InsuranceCardInfo = jsonResult.data;
              $scope.ShowPayment =
                jsonResult.data.is_payment === "Y" ? true : false;
            }

            // $scope.InsuranceCardInfo = "";
            // $scope.InsuranceCardInfo = response.data.d[0];

            // if (
            //   (val != "" && response.data.d.length == 0) ||
            //   response.data.d == null
            // ) {
            //   $scope.NotShowCard = "ไม่แสดงข้อมูลบัตรผู้เอาประกันภัย";
            // } else {
            //   $scope.NotShowCard = "";
            // }

            $ionicLoading.hide();
          },
          function(response) {
            $ionicLoading.hide();
            if (response.status == 401) {
              TMLTHService.unauthorizedService();
            } else {
              alert("Fail GetInsuredCard: " + response.message);
            }
          }
        );
      } catch (err) {
        $ionicLoading.hide();
        alert("Error : " + err.message);
      }
    }
  }

  function fnGetInsureBaseInfo(val) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      $http({
        url: URLInsure + "/GetInsuranceInfo_New",
        method: "POST",
        data: { strPolicyNo: EncryptDecryptData.Encrypt(val) }
      }).then(
        function(response) {
          // console.log(response)

          $scope.InsuranceBaseInfo = "";
          $scope.InsuranceBaseInfo = response.data.d[0];
          $scope.InsuranceInfo = response.data.d;

          $scope.BaseInfoListTel = "OK";
          $scope.BaseInfoOccdate = $scope.InsuranceBaseInfo.sOccdate;
          $scope.BaseInfoBillfreq = $scope.InsuranceBaseInfo.Billfreq;
          $scope.BaseInfoPtdate = $scope.InsuranceBaseInfo.sPtdate;
          $scope.BaseInfoAgnt_name = $scope.InsuranceBaseInfo.Agnt_name;
          $scope.BaseInfoBasic_instprem =
            $scope.InsuranceBaseInfo.Basic_instprem;

          str_start_agreement = $scope.InsuranceBaseInfo.start_agreement;
          str_end_agreement = $scope.InsuranceBaseInfo.end_agreement;

          // console.log(str_start_agreement + '-' + str_end_agreement)
          // debugger;
          $scope.SHOW_COLUMN_ILP = $scope.InsuranceBaseInfo.SHOW_COLUMN_ILP;

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail GetInsuranceInfo_New: " + response.message);
          }
        }
      );
    } catch (err) {
      // alert("Error : " + err.message);
    }
  }

  function fnGetInsureClaim(val) {
    //If  Can View
    try {
      $http({
        url: URLInsure + "/GetPolicyInfo_New",
        method: "POST",
        data: { strPolicyNo: EncryptDecryptData.Encrypt(val) }
      }).then(
        function(response) {
          var objGetPolicyInfoResult = response.data.d[0];
          var strChdr_pstatcode = objGetPolicyInfoResult.Chdr_pstatcode;

          $scope.MsgNoDataClaim = "";
          //CF,ET,FF,FS,LA,PU,SU : Cannot View Claim
          if (
            strChdr_pstatcode == "CF" ||
            strChdr_pstatcode == "ET" ||
            strChdr_pstatcode == "FF" ||
            strChdr_pstatcode == "FS" ||
            strChdr_pstatcode == "LA" ||
            strChdr_pstatcode == "PU" ||
            strChdr_pstatcode == "SU"
          ) {
            //alert('Get Data');
            $scope.MsgNoDataClaim = "ไม่มีข้อมูล";
            //fnGetInsureClaimInfo(val);
          } else {
            //$scope.MsgNoDataClaim = "ไม่มีข้อมูล";
            fnGetInsureClaimInfo(val);
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail GetPolicyInfo_New: " + response.message);
          }
        }
      );
    } catch (err) {
      // alert("Error : " + err.message);
    }
  }

  function fnGetInsureClaimInfo(val) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    try {
      $http({
        url: URLInsure + "/GetInsuranceGroupingClaim_New",
        method: "POST",
        data: { strPolicyNo: EncryptDecryptData.Encrypt(val) }
      }).then(
        function(response) {
          $scope.InsuranceGroupingClaim = response.data.d;

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail GetInsuranceGroupingClaim_New: " + response.message);
          }
        }
      );
    } catch (err) {
      // alert("Error : " + err.message);
    }

    try {
      $http({
        url: URLInsure + "/GetInsuranceClaim_New",
        method: "POST",
        data: { strPolicyNo: EncryptDecryptData.Encrypt(val) }
      }).then(
        function(response) {
          $scope.InsuranceClaim = response.data.d;
          if ($scope.InsuranceClaim.length == 0) {
            $scope.MsgNoDataClaim = "ไม่มีรายการข้อมูล";
          }

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail GetInsuranceClaim_New: " + response.message);
          }
        }
      );
    } catch (err) {
      // alert("Error : " + err.message);
    }
  }

  /**************************************************************************/
  /*Claim Detail*/
  /*alert($scope.id);
                 alert($scope.Policy_no);
                 alert($scope.IncidentDT);
                 alert($scope.rgpnum);*/
  if (
    angular.isDefined($scope.id) &&
    angular.isDefined($scope.Policy_no) &&
    angular.isDefined($scope.IncidentDT) &&
    angular.isDefined($scope.rgpnum)
  ) {
    //alert('OK');

    $scope.ObjIncident_dt = "Loading...";
    $scope.ObjClaim_recv_dt = "Loading...";
    $scope.ObjHospital = "Loading...";
    $scope.ObjLongdesc = "Loading...";
    $scope.ObjChannel = "Loading...";
    $scope.ObjDecision_status = "Loading...";
    $scope.ObjPaym_amt = "Loading...";
    $scope.ObjApprvDate = "Loading...";
    $scope.ObjReason = "Loading...";

    $scope.testnull = null;

    try {
      $http({
        url: URLInsure + "/GetInsuranceClaimDetail_New",
        method: "POST",
        data: {
          strPolicyNo: EncryptDecryptData.Encrypt($scope.Policy_no),
          strIncidentDT: EncryptDecryptData.Encrypt($scope.IncidentDT),
          intRgpnum: $scope.rgpnum,
          strPagesize: $scope.testnull
        }
      }).then(
        function(response) {
          $scope.InsuranceClaimDetail = response.data.d[0];
          $scope.ListInsuranceClaimDetail = response.data.d;

          if ($scope.InsuranceClaimDetail == null) {
            try {
              $http({
                url: URLInsure + "/GetInsuranceClaim_New",
                method: "POST",
                data: {
                  strPolicyNo: EncryptDecryptData.Encrypt($scope.Policy_no)
                }
              }).then(
                function(response) {
                  $scope.ObjIncident_dt = "-";
                  $scope.ObjClaim_recv_dt = "-";
                  $scope.ObjHospital = "-";
                  $scope.ObjLongdesc = "-";
                  $scope.ObjChannel = "-";
                  $scope.ObjDecision_status = "-";
                  $scope.ObjPaym_amt = "-";
                  $scope.ObjReason = "-";

                  $scope.InsClaimDtl = response.data.d[$scope.id];

                  $scope.ObjHospital = $scope.InsClaimDtl.Hospital;

                  /*Set Value To binding*/
                  // ====== Edit by Tirachit B. @20170629 ======= //
                  // $scope.ObjIncident_dt = fnConvertYYYYMMDDDateToString($scope.InsClaimDtl.Incident_dt);
                  // $scope.ObjClaim_recv_dt = fnConvertYYYYMMDDDateToString($scope.InsClaimDtl.Claim_recv_dt);
                  // $scope.ObjPaym_amt = fnNumberWithCommas($scope.InsClaimDtl.Paym_amt);
                  // $scope.ObjHospital = $scope.InsClaimDtl.Hospital;
                  // $scope.ObjLongdesc = $scope.InsClaimDtl.Longdesc;
                  // $scope.ObjDecision_status = $scope.InsClaimDtl.Decision_status;
                  // $scope.ObjApprvDate = '-';
                  // $scope.ObjReason = $scope.InsClaimDtl.Reason;
                  // $scope.ObjChannel = '-';

                  $scope.ObjIncident_dt = $scope.InsClaimDtl.Incident_dt;
                  $scope.ObjClaim_recv_dt = $scope.InsClaimDtl.Claim_recv_dt;
                  $scope.ObjPaym_amt = $scope.InsClaimDtl.Paym_amt;
                  //alert($scope.ObjPaym_amt);
                  $scope.ObjHospital = $scope.InsClaimDtl.Hospital;
                  $scope.ObjLongdesc = $scope.InsClaimDtl.Longdesc;
                  $scope.ObjDecision_status =
                    $scope.InsClaimDtl.Decision_status;
                  $scope.ObjApprvDate = "-";
                  $scope.ObjReason = $scope.InsClaimDtl.Reason;
                  $scope.ObjChannel = "-";
                  // ====== End edit by Tirachit B. @20170629 ======= //
                  /*End Set Value To binding*/
                },
                function(response) {
                  if (response.status == 401) {
                    TMLTHService.unauthorizedService();
                  } else {
                    alert("Fail GetInsuranceClaim_New: " + response.message);
                  }
                }
              );
            } catch (err) {
              // alert("Error : " + err.message);
            }
          } else {
            $scope.ObjIncident_dt = "-";
            $scope.ObjClaim_recv_dt = "-";
            $scope.ObjHospital = "-";
            $scope.ObjLongdesc = "-";
            $scope.ObjChannel = "-";
            $scope.ObjDecision_status = "-";
            $scope.ObjPaym_amt = "-";
            $scope.ObjApprvDate = "-";
            $scope.ObjReason = "-";
            // $scope.ObjIncident_dt = fnConvertYYYYMMDDDateToString($scope.InsuranceClaimDetail.OccurDate);
            // $scope.ObjClaim_recv_dt = fnConvertYYYYMMDDDateToString($scope.InsuranceClaimDetail.AcknowledgeDate);
            // $scope.ObjHospital = $scope.InsuranceClaimDetail.HospitalNM;
            // $scope.ObjLongdesc = $scope.InsuranceClaimDetail.Contract;
            // $scope.ObjChannel = $scope.InsuranceClaimDetail.Channel;
            // $scope.ObjDecision_status = $scope.InsuranceClaimDetail.AnalystStatus;
            // $scope.ObjPaym_amt = fnNumberWithCommas($scope.InsuranceClaimDetail.PaymentAmt);
            // $scope.ObjApprvDate = fnConvertYYYYMMDDDateToString($scope.InsuranceClaimDetail.ApprvDate);
            // $scope.ObjReason = $scope.InsuranceClaimDetail.Reason;
            // OccurDate = Incident_dt
            // ====== Edit by Tirachit B. @20170629 ======= //

            // Set Value Header
            $scope.ObjIncident_dt = $scope.InsuranceClaimDetail.OccurDate;
            $scope.ObjClaim_recv_dt =
              $scope.InsuranceClaimDetail.AcknowledgeDate;
            $scope.ObjHospital = $scope.InsuranceClaimDetail.HospitalNM;
            $scope.ObjLongdesc = $scope.InsuranceClaimDetail.Contract;
            $scope.ObjChannel = $scope.InsuranceClaimDetail.Channel;
            $scope.ObjDecision_status =
              $scope.InsuranceClaimDetail.AnalystStatus;
            $scope.ObjPaym_amt = $scope.InsuranceClaimDetail.PaymentAmt;
            if (
              $scope.InsuranceClaimDetail.ApprvDate != null &&
              $scope.InsuranceClaimDetail.ApprvDate != ""
            ) {
              $scope.ObjApprvDate = $scope.InsuranceClaimDetail.ApprvDate;
            }
            $scope.ObjReason = $scope.InsuranceClaimDetail.Reason;
            // ====== End edit by Tirachit B. @20170629 ======= //
            // End Set Value Header
          }
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      // alert("Error : " + err.message);
    }
  } else {
  }
  /**************************************************************************/

  $scope.ConvertToPhoneFormat = function(val) {
    var retVal = "";

    var AllVal = val.split(",");

    for (i = 0; i <= AllVal.length - 1; i++) {
      AllVal[i] = AllVal[i].trim();

      if (AllVal[i].length == 9) {
        var preCode = "";
        preCode = AllVal[i].substring(0, 2);

        if (preCode == "02") {
          AllVal[i] = AllVal[i].replace(/(\d{2})(\d{3})(\d{4})/, "$1-$2-$3");
        } else {
          AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{3})/, "$1-$2-$3");
        }

        //AllVal[i] = AllVal[i];
      } else if (AllVal[i].length == 10) {
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      } else {
        //AllVal[i] = AllVal[i];
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d)/, "$1-$2-$3");
      }

      retVal = retVal + AllVal[i] + ",";
    }

    if (retVal != "") {
      retVal = retVal.substring(0, retVal.length - 1);
    }

    return retVal;
  };

  $scope.BackToPrevious = function() {
    if (strLevelCo == 61) {
      $state.go("app.CustomerList", { ascode: strASCode });
    } else if (strLevelCo == 62) {
      $state.go("app.CustomerList", { ascode: strSelectedASCodeAgent });
    } else if (strLevelCo == 63) {
      $state.go("app.CustomerList", { ascode: strSelectedASCodeAgent });
    }
  };
}).directive("flipContainer", function() {
  return {
    restrict: "C",
    link: function($scope, $elem, $attrs) {
      $scope.flip = function() {
        $elem.toggleClass("flip");
      };
    }
  };
});
