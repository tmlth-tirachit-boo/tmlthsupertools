var app = angular.module("TokioMarineMobileApp", ["ionic", "ion-autocomplete"]);

/******************************************************************************/

/*customer*/
var strIDCard = "";
var strClntnum = "";
var strChdrnum = "";
var strQuestion = "";
var strAnswer = "";
var strPassword = "";
var strUserid = "";
var strName_ins = "";
var strLastname_ins = "";
var strMobile_no = "";
var dtMobile_dt = "";
var strPrev_mobile = "";
var strEmail = "";
var dtEmail_dt = "";
var strPrev_email = "";
var strAgreement = "";
var dtUpddate = "";
var dtRegister_dt = "";
var strPhone = "";
var strPrev_phone = "";
var dtPhone_dt = "";
var strReset_password = "";
var guidReset_code = "";
var dtReset_date = "";
var strSend_life_asia = "";
var dtSend_dt = "";
var strUserImagePath = "";
var strUniqueCode = "";
var strCltdob_ins = "";
var str_start_agreement = "";
var str_end_agreement = "";

/*Agent*/
var strIsAgent = "Y";
var strAgentLv = "";
var strASCode = "";
var strASName = "";
var strASSName = "";
var strASTel = "";
var strASInfo = "";
var strASStatus = "";
var strPostnco = "";
var strUnitco = "";
var strUntStatus = "";
var strASPost = "";
var strLevelCo = "";
var strALCode = "";
var strALName = "";
var strALSName = "";
var strALTel = "";
var strALInfo = "";
var strALStatus = "";
var strALPost = "";
var strGRPCode = "";
var strGRPStatus = "";
var strAECode = "";
var strAEName = "";
var strAESName = "";
var strAETel = "";
var strAEInfo = "";
var strAEPost = "";
var strAreaCo = "";
var strAEStatus = "";
var strUpdated_dt = "";
var strMktchan = "";
var strASJoinDate = "";
var strALJoinDate = "";
var strAEJoinDate = "";
var strChaASCode = "";
var strChaPassword = "";
var strChaUser = "";
var strNLevel = "";
var strRegDte = "";
var strRegCo = "";
var strForgCo = "";
var strMktChan_TUserPass = "";
var strUser_Type = "";
var strAgent_Statement = "";

var strSelectedASCode63 = "";
var strSelectedUnit = "";
var strSelectedASCodeAgent = "";

/*Hospital*/
var strHospitalProvSelected = "";

var strPaymentType = "";

/*class cUSRInfo {
    strIDCard,
    strClntnum
};
var USRInfo = new cUSRInfo;
*/

app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state("app", {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: "AppCtrl"
    })

    .state("app.search", {
      url: "/search",
      views: {
        menuContent: {
          templateUrl: "templates/search.html"
        }
      }
    })

    .state("app.browse", {
      url: "/browse",
      views: {
        menuContent: {
          templateUrl: "templates/browse.html"
        }
      }
    })
    .state("app.playlists", {
      url: "/playlists",
      views: {
        menuContent: {
          templateUrl: "templates/playlists.html",
          controller: "PlaylistsCtrl"
        }
      }
    })

    .state("app.single", {
      url: "/playlists/:playlistId",
      views: {
        menuContent: {
          templateUrl: "templates/playlist.html",
          controller: "PlaylistCtrl"
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise("/app/playlists");

  /*$scope.strIDCard = '';
$scope.strClntnum = '';
$scope.strChdrnum = '';
$scope.strQuestion = '';
$scope.strAnswer = '';
$scope.strPassword = '';
$scope.strUserid = '';
$scope.strName_ins = '';
$scope.strLastname_ins = '';
$scope.strMobile_no = '';
$scope.dtMobile_dt = '';
$scope.strPrev_mobile = '';
$scope.strEmail = '';
$scope.dtEmail_dt = '';
$scope.strPrev_email = '';
$scope.strAgreement = '';
$scope.dtUpddate = '';
$scope.dtRegister_dt = '';
$scope.strPhone = '';
$scope.strPrev_phone = '';
$scope.dtPhone_dt = '';
$scope.strReset_password = '';
$scope.guidReset_code = '';
$scope.dtReset_date = '';
$scope.strSend_life_asia = '';
$scope.dtSend_dt = '';

$scope.strUserImagePath = '';
$scope.strUniqueCode = '';
*/
});
