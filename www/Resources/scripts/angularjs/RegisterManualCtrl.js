var ActionVal = ActRegister;

CPApp.controller("RegisterManualCtrl", function(
  $scope,
  $stateParams,
  $http,
  $state,
  $timeout,
  $ionicLoading,
  $ionicSlideBoxDelegate
) {
  //console.log("RegisterManualCtrl");

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $timeout(function() {
    $ionicLoading.hide();
    //$scope.stooges = [{name: 'Moe'}, {name: 'Larry'}, {name: 'Curly'}];
  }, 2000);

  //alert("Register Manual");

  // Called to navigate to the main app
  $scope.startApp = function() {
    $state.go("Login");
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
  };

  //***************** For Download App ****************************//
  $scope.getApp = function() {
    var strURL = "";
    //alert(ionic.Platform.platform());
    if (ionic.Platform.platform() == "ios") {
      strURL = "https://goo.gl/HdS1HR";
      window.open(encodeURI(strURL), "_system", "location=yes");
    } else if (ionic.Platform.platform() == "android") {
      strURL = "https://goo.gl/3rvTUD";
      window.open(encodeURI(strURL), "_system", "location=yes");
    } else {
      alert("Sorry, This function not support your platform");
    }
  };
  //***************** For Download App ****************************//
});
