//var app = angular.module('ngpdfviewerApp', [ 'ngPDFViewer' ]);

CPApp.controller('PDFCtrl', [ '$scope', 'PDFViewerService', function($scope, pdf) {
    $scope.viewer = pdf.Instance("viewer");

    $scope.nextPage = function() {
        $scope.viewer.nextPage();
    };

    $scope.prevPage = function() {
        $scope.viewer.prevPage();
    };

    $scope.pageLoaded = function(curPage, totalPages) {
        $scope.currentPage = curPage;
        $scope.totalPages = totalPages;
    };
}]);



/*CPApp.controller('PDFCtrl', function($scope,$stateParams,$http, $state,$timeout, $ionicLoading) {
  console.log('PDFCtrl');


alert('xxx');
 $scope.pdfUrl = 'http://testweb1/CustomerPortal/ForDownload/DownloadDocument/Icon_touch_point.pdf';

    $scope.viewer = pdf.Instance("viewer");

    $scope.nextPage = function() {
        $scope.viewer.nextPage();
    };

    $scope.prevPage = function() {
        $scope.viewer.prevPage();
    };

    $scope.pageLoaded = function(curPage, totalPages) {
        $scope.currentPage = curPage;
        $scope.totalPages = totalPages;
    };


});*/
