CPApp.controller("BranchCtrl", function(
  $scope,
  $stateParams,
  $http,
  $timeout,
  $ionicLoading,
  TMLTHService
) {
  // console.log("BranchCtrl");

  // Setup the loader
  $ionicLoading.show({
    content: "Loading",
    animation: "fade-in",
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  try {
    $http({
      url: URLBranch + "/GetHeadOffice",
      method: "POST",
      data: { strLng: lng }
    }).then(
      function(response) {
        $scope.HeadOffice = response.data.d;
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Fail");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  try {
    $http({
      url: URLBranch + "/GetBranchProvince",
      method: "POST",
      data: { strLng: lng }
    }).then(
      function(response) {
        $scope.ListBranchProv = response.data.d;

        $timeout(function() {
          $ionicLoading.hide();
        }, 2000);
      },
      function(response) {
        if (response.status == 401) {
          TMLTHService.unauthorizedService();
        } else {
          alert("Fail");
        }
      }
    );
  } catch (err) {
    alert("Error : " + err.message);
  }

  $scope.ddlSelectProv = ""; //Insure ID
  $scope.GetBranchByProv = function(val) {
    // Setup the loader
    $ionicLoading.show({
      content: "Loading",
      animation: "fade-in",
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    /***************************************************************************/
    /*Branch*/
    try {
      $http({
        url: URLBranch + "/GetBranchByProvince",
        method: "POST",
        data: { strLng: lng, strProv: val }
      }).then(
        function(response) {
          $scope.ListBranchByProv = response.data.d;

          $timeout(function() {
            $ionicLoading.hide();
          }, 2000);
        },
        function(response) {
          if (response.status == 401) {
            TMLTHService.unauthorizedService();
          } else {
            alert("Fail");
          }
        }
      );
    } catch (err) {
      alert("Error : " + err.message);
    }
  };

  $scope.ConvertToPhoneFormat = function(val) {
    var retVal = "";

    var AllVal = val.split(",");

    for (i = 0; i <= AllVal.length - 1; i++) {
      AllVal[i] = AllVal[i].trim();

      //AllVal[i] = AllVal[i].replace("-", "");

      if (AllVal[i].length == 9) {
        var preCode = "";
        preCode = AllVal[i].substring(0, 2);

        if (preCode == "02") {
          AllVal[i] = AllVal[i].replace(/(\d{2})(\d{3})(\d{4})/, "$1-$2-$3");
        } else {
          AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{3})/, "$1-$2-$3");
        }

        //AllVal[i] = AllVal[i];
      } else if (AllVal[i].length == 10) {
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
      } else {
        //AllVal[i] = AllVal[i];
        AllVal[i] = AllVal[i].replace(/(\d{3})(\d{3})(\d)/, "$1-$2-$3");
      }

      retVal = retVal + AllVal[i] + ",";
    }

    if (retVal != "") {
      retVal = retVal.substring(0, retVal.length - 1);
    }

    return retVal;
  };

  //Param To News Detail
  $scope.id = $stateParams.id;
  $scope.Policy_no = $stateParams.Policy_no;
});

/*
$scope.HeadOfficeInfo = [
  {
    id: 1,
    HeadOfficeName:"บริษัท โตเกียวมารีนประกันชีวิต (ประเทศไทย) จำกัด มหาชน",
    HeadOfficeAddress: "เลขที่ 1 อาคารเอ็มไพร์ทาวเวอร์ ชั้น 26 ถนนสาทรใต้ แขวงยานนาวา เขตสาทร กรุงเทพฯ 10120 ",
    HeadOfficePhone:"02-650-1400",
    HeadOfficeFax:" 02-650-1402",
    HeadOfficeEmail:"CSC@tokiomarinelife.co.th",
    latitude:"14.31703",
    longitude:"100.566581"
  }
]

$scope.ListBranchInfo = [
  {
    id: 1,
    BranchName:"บริษัท โตเกียวมารีนประกันภัย (ประเทศไทย) จำกัด สาขา พระนครศรีอยุธยา",
    BranchAddress: "29/4 หมู่ที่ 2 ต.ธนู อ.อุทัย จ.พระนครศรีอยุธยา",
    BranchPhone:"02xxxxxxx",
    BranchFax:"02yyyyyyy",
    BranchEmail:"xxx@tokiomarinelife.co.th",
    latitude:"14.31703",
    longitude:"100.566581"
  },
  {
    id: 2,
    BranchName:"บริษัท โตเกียวมารีนประกันภัย (ประเทศไทย) จำกัด สาขา พิษณุโลก",
    BranchAddress: "343/5 หมู่ที่ 7 ถ.มิตรภาพ ต.สมอแข อ.เมืองพิษณุโลก จ.พิษณุโลก",
    BranchPhone:"02xxxxxxx",
    BranchFax:"02yyyyyyy",
    BranchEmail:"xxx@tokiomarinelife.co.th",
    latitude:"16.8833",
    longitude:"100.3167"
  },
  {
    id: 3,
    BranchName:"บริษัท โตเกียวมารีนประกันภัย (ประเทศไทย) จำกัด สาขา นครปฐม",
    BranchAddress: "908 ถ.เพชรเกษม ต.ห้วยจรเข้ อ.เมืองนครปฐม จ.นครปฐม",
    BranchPhone:"02xxxxxxx",
    BranchFax:"02yyyyyyy",
    BranchEmail:"xxx@tokiomarinelife.co.th",
    latitude:"13.85",
    longitude:"100.167"
  }
]
*/
